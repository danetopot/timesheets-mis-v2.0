USE [DP_TIMESHEET_MIS_v2]
GO
/****** Object:  StoredProcedure [dbo].[Sp_SearchTimesheet]    Script Date: 19/02/2019 18:39:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SearchTimesheet] 
@ConsultantName varchar(20) = null,
@Period int = null,
@Project varchar(20) = null
AS
BEGIN
	SET NOCOUNT ON;

	SELECT T1.ConsultantId, T1.ApprovedById, T1.TimesheetStatus, T1.ProjectId,
		SUM(T2.HoursWorked) AS HoursWorked,
		CONCAT(T3.FirstName,' ', T3.Surname) AS ConsultantName,
		T4.Name AS Period,
		T5.Name AS ProjectName
		FROM TimesheetHeader T1 
		INNER JOIN TimeSheetDetail T2 ON T2.TimesheetHeaderId=T1.Id
		INNER JOIN AspNetUsers T3 ON T1.ConsultantId=T3.Id
		INNER JOIN Period T4 ON T4.Id = T1.PeriodId
		INNER JOIN Project T5 ON T5.Id = T1.ProjectId
		
		WHERE 
		(T3.FirstName LIKE '%' + ISNULL(@ConsultantName, T3.FirstName) + '%') AND
		(T3.Surname LIKE '%' + ISNULL(@ConsultantName, T3.Surname) + '%') AND
		(T4.Id = ISNULL(@Period, T4.Id) )
		
		GROUP BY 
		T1.ConsultantId, T1.PeriodId, T1.ApprovedById, T1.ProjectId,T1.TimesheetStatus, 
		T3.FirstName, T3.Surname, T4.Name,   T5.Name



	
END