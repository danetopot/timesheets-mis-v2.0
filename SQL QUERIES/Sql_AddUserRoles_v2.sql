USE [DP_TIMESHEET_MIS_v2]
GO

INSERT INTO [dbo].[AspNetRoles]
           ([Id]
           ,[Name]
           ,[NormalizedName]
           ,[ConcurrencyStamp])
     VALUES
           ('3702e9fe-cc74-4824-99ae-7d2ce5bf0523'
           ,'Consultant'
           ,'CONSULTANT'
           ,'3d3d1234-5bdc-4b09-9366-40b5b8a64e50')

INSERT INTO [dbo].[AspNetRoles]
           ([Id]
           ,[Name]
           ,[NormalizedName]
           ,[ConcurrencyStamp])
     VALUES
           ('3702e9fe-cc33-4824-99ae-7d2ce5bf0523'
           ,'Supervisor'
           ,'SUPERVISOR'
           ,'3d3d1234-5bdc-4b669-9366-40b5b8a64e50')

INSERT INTO [dbo].[AspNetRoles]
           ([Id]
           ,[Name]
           ,[NormalizedName]
           ,[ConcurrencyStamp])
     VALUES
           ('3702e9fe-cc74-4824-99ae-7e2ce5bf0523'
           ,'Administrator'
           ,'ADMINISTRATOR'
           ,'3d3d1434-5bdc-4b09-9366-40b5b8a64e50')



INSERT INTO [dbo].[AspNetRoles]
           ([Id]
           ,[Name]
           ,[NormalizedName]
           ,[ConcurrencyStamp])
     VALUES
           ('3702e9fe-cc74-4824-39ae-7e2ff5bf0523'
           ,'Administrator'
           ,'DEVELOPER'
           ,'3d3d3334-5bdc-4b09-1122-40b5b8a64e50')
GO


