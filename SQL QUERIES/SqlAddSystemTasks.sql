USE [DP_TIMESHEET_MIS_v2]
GO

INSERT INTO [dbo].[SystemTask]
           ([Name])
     VALUES
	 ('View Dashboard')

INSERT INTO [dbo].[SystemTask]
           ([Name])
     VALUES
           ('Create Timesheet')
		   
INSERT INTO [dbo].[SystemTask]
           ([Name])
     VALUES
           ('Edit Timesheet')

INSERT INTO [dbo].[SystemTask]
           ([Name])
     VALUES
           ('View Timesheet')

INSERT INTO [dbo].[SystemTask]
           ([Name])
     VALUES
           ('Delete Timesheet')

INSERT INTO [dbo].[SystemTask]
           ([Name])
     VALUES
           ('Approve/Reject Timesheet')

INSERT INTO [dbo].[SystemTask]
           ([Name])
     VALUES
           ('Search User')

INSERT INTO [dbo].[SystemTask]
           ([Name])
     VALUES
           ('Assign User Roles')

INSERT INTO [dbo].[SystemTask]
           ([Name])
     VALUES
           ('Configure System Settings')

INSERT INTO [dbo].[SystemTask]
           ([Name])
     VALUES
           ('Generate Reports')

GO


