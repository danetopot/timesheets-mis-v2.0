USE [DP_TIMESHEET_MIS_v2]
GO

INSERT INTO [dbo].[SystemCode]
           ([Code]
           ,[Description]
           ,[SystemModuleId])
     VALUES
           ('Consultant Categories'
           ,'Consultant Type'
           ,1)

INSERT INTO [dbo].[SystemCode]
           ([Code]
           ,[Description]
           ,[SystemModuleId])
     VALUES
           ('Consultant Position Categories'
           ,'Consultant Position'
           ,4)
		   

INSERT INTO [dbo].[SystemCode]
           ([Code]
           ,[Description]
           ,[SystemModuleId])
     VALUES
           ('Department Categories'
           ,'Departments'
           ,2)

INSERT INTO [dbo].[SystemCode]
           ([Code]
           ,[Description]
           ,[SystemModuleId])
     VALUES
           ('Project Role Categories'
           ,'Role in Project'
           ,4)

INSERT INTO [dbo].[SystemCode]
           ([Code]
           ,[Description]
           ,[SystemModuleId])
     VALUES
           ('Internal External Categories'
           ,'Internal or External Consultant'
           ,4)


INSERT INTO [dbo].[SystemCode]
           ([Code]
           ,[Description]
           ,[SystemModuleId])
     VALUES
           ('SkillSet Categories'
           ,'Skill Set - Tech or Operations'
           ,4)

INSERT INTO [dbo].[SystemCode]
           ([Code]
           ,[Description]
           ,[SystemModuleId])
     VALUES
           ('Report Categories'
           ,'Report Types and Categories'
           ,7)

INSERT INTO [dbo].[SystemCode]
           ([Code]
           ,[Description]
           ,[SystemModuleId])
     VALUES
           ('Department Categories'
           ,'Department Types and Categories'
           ,6)
GO


