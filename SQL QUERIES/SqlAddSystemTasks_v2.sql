USE [DP_TIMESHEET_MIS_v2]
GO

/*
/** Parent Tasks/Core Tasks **/
INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (null, 'Dashboard', 1)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (null, 'Timesheet', 1)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (null, 'Manage Users', 1)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (null, 'Manage Profiles', 1)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (null, 'Settings', 1)
INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (null, 'Reports', 1)

**/
		   
/** Child Tasks/Sibling Tasks **/

/** 1. Dashboard **/
INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (11, 'View Dashboard', 1)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (11, 'Pivot Tables', 1)

/** 2. Timesheet **/
INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (12, 'Create', 1)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (12, 'Modify', 2)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (12, 'View', 3)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (12, 'Delete', 4)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (12, 'Send For Approval', 5)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (12, 'Approve', 6)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (12, 'Print', 7)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (12, 'Search', 8)

/** 3. Manage Users **/
INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (13, 'Create', 1)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (13, 'Manage Role', 2)

/** 4. Manage Profiles **/
INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (14, 'Create', 1)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (14, 'Modify', 2)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (14, 'View', 3)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (14, 'Delete', 4)


/** 5. Settings **/
INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (15, 'Create', 1)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (15, 'Modify', 2)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (15, 'View', 3)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (15, 'Delete', 4)


/** 6. Reports **/
INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (16, 'Run', 1)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (16, 'View', 2)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (16, 'Share', 3)

INSERT INTO [dbo].[SystemTask]
           ([ParentId]
           ,[Name]
           ,[Order])
     VALUES
           (16, 'Print', 4)
GO
