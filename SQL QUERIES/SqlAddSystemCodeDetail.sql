USE [DP_TIMESHEET_MIS_v2]
GO

/* Consultant Categories */
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (1,
           'Desk Based',
           3)

INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (1,
           'Field Based',
           2)

INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (1,
           'Skype Based',
           3)

/* Consultant Position Categories */
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (2,
           'Driver',
           1)

INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (2,
           'Office Administrator',
           2)
		   
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (2,
           'MIS Advisor',
           3)
		   
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (2,
           'MIS Specialist',
           3)	
		   	   
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (2,
           'Head Of Operations',
           4)

		   
/* Report Type Categories */
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (6,
           'Consultant Level',
           1)

INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (6,
           'Department Level',
           2)

INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (6,
           'Project Level',
           3)


/* Department Categories */
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (7,
           'Management Information System (MIS)',
           1)
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (7,
           'Social Policy (SP)',
           2)
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (7,
           'Social & Economic Assistance (SEA)',
           3)

/* Roles in Prjoect Categories */
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (4,
           'System Analyst',
           1)

INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (4,
           'System Designer',
           12)

INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (4,
           'System Developer',
           3)

/* Internal & External Categories */
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (5,
           'Internal',
           1)
 INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (5,
           'External',
           2)

/* Skill Set Categories */
INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (4,
           'Operations',
           1)
 INSERT INTO [dbo].[SystemCodeDetails]
           ([SystemCodeId]
           ,[Code]
           ,[OrderNo])
     VALUES
           (4,
           'Technical',
           2)
GO


