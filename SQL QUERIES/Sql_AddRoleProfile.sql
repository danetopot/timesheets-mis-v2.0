USE [DP_TIMESHEET_MIS_v2]
GO

/* Guest [View Dashboard] */
INSERT INTO [dbo].[RoleProfiles]
           ([TaskId]
           ,[RoleId])
     VALUES
           (1,'3702e9cd-cc74-4824-99ae-7d2ce5bf05e3')
GO

/* Consultant [View Dashboard, Create/View/Edit/Delete Timesheet] */
INSERT INTO [dbo].[RoleProfiles]
           ([TaskId]
           ,[RoleId])
     VALUES
           (2,'3702e9fe-cc74-4824-99ae-7d2ce5bf0523')
INSERT INTO [dbo].[RoleProfiles]
           ([TaskId]
           ,[RoleId])
     VALUES
           (3,'3702e9fe-cc74-4824-99ae-7d2ce5bf0523')
INSERT INTO [dbo].[RoleProfiles]
           ([TaskId]
           ,[RoleId])
     VALUES
           (4,'3702e9fe-cc74-4824-99ae-7d2ce5bf0523')
INSERT INTO [dbo].[RoleProfiles]
           ([TaskId]
           ,[RoleId])
     VALUES
           (5,'3702e9fe-cc74-4824-99ae-7d2ce5bf0523')
GO
/* Supervisor [View Dashboard, Create/View/Edit/Delete Timesheet] */
INSERT INTO [dbo].[RoleProfiles]
           ([TaskId]
           ,[RoleId])
     VALUES
           (6,'3702e9fe-cc33-4824-99ae-7d2ce5bf0523')

/* Admin [Assign User Roles, Configure System Settings] */
INSERT INTO [dbo].[RoleProfiles]
           ([TaskId]
           ,[RoleId])
     VALUES
           (7,'3702e9fe-cc74-4824-99ae-7e2ce5bf0523')
INSERT INTO [dbo].[RoleProfiles]
           ([TaskId]
           ,[RoleId])
     VALUES
           (8,'3702e9fe-cc74-4824-99ae-7e2ce5bf0523')
INSERT INTO [dbo].[RoleProfiles]
           ([TaskId]
           ,[RoleId])
     VALUES
           (9,'3702e9fe-cc74-4824-99ae-7e2ce5bf0523')



