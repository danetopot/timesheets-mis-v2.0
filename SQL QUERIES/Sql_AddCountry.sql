USE [DP_TIMESHEET_MIS_v2]
GO

INSERT INTO [dbo].[Country]
           ([Abbreviation]
           ,[Name])
     VALUES
           ('KE',
           'Kenya')

INSERT INTO [dbo].[Country]
           ([Abbreviation]
           ,[Name])
     VALUES
           ('UG',
           'Uganda')

INSERT INTO [dbo].[Country]
           ([Abbreviation]
           ,[Name])
     VALUES
           ('TZ',
           'Tanzania')
GO


