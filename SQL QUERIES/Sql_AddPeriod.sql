USE [DP_TIMESHEET_MIS]
GO

INSERT INTO [dbo].[Period]
           ([PeriodStartDate]
           ,[PeriodStopDate]
           ,[PeriodDescription])
     VALUES
           (convert(datetime,'01/01/2019',105),
		   convert(datetime,'31/01/2019',105),
		   'January 2019 - February 2019')