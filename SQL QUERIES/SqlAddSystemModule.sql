USE [DP_TIMESHEET_MIS_v2]
GO


INSERT INTO [dbo].[SystemModule]
           ([Name])
     VALUES
           ('Login Module')

INSERT INTO [dbo].[SystemModule]
           ([Name])
     VALUES
           ('Registration Module')

INSERT INTO [dbo].[SystemModule]
           ([Name])
     VALUES
           ('Dashboard Module')

INSERT INTO [dbo].[SystemModule]
           ([Name])
     VALUES
           ('Timesheet Module')

INSERT INTO [dbo].[SystemModule]
           ([Name])
     VALUES
           ('Report Module')

INSERT INTO [dbo].[SystemModule]
           ([Name])
     VALUES
           ('System Administration Module')

INSERT INTO [dbo].[SystemModule]
           ([Name])
     VALUES
           ('System Security Module')

GO


