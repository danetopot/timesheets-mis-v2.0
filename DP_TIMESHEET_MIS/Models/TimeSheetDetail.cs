﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DP_TIMESHEET_MIS.Models
{
    public class TimeSheetDetail
    {
        [Key]
        public string Id { get; set; }

        public TimesheetHeader TimesheetHeader { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime DateFilled { get; set; }

        public int HoursWorked { get; set; }

        [ForeignKey("SiteId")]
        public SystemCodeDetail Site { get; set; }

        public Project Project { get; set; }

        public string Location { get; set; }

        public string TaskDescription { get; set; }

        public DateTime DateCreated { get; set; }

        public ApplicationUser CreatedBy { get; set; }

    }
}
