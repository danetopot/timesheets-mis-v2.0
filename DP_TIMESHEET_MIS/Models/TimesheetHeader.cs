﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DP_TIMESHEET_MIS.Models
{
    public class TimesheetHeader
    {
        [Key]
        public string Id { get; set; }
        
        [ForeignKey("PeriodId")]
        public Period Period { get; set; }

        public ApplicationUser Consultant { get; set; }

        public string TimesheetStatus { get; set; }

        [ForeignKey("ProjectId")]
        public Project Project { get; set; }

        public ApplicationUser ApprovedBy { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? DateApproved { get; set; }

        public ApplicationUser RejectedBy { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? DateRejected { get; set; }

        public string RejectReason { get; set; }

        public bool IsActive { get; set; }

        public DateTime DateCreated { get; set; }

        public ApplicationUser CreatedBy { get; set; }
    }
}
