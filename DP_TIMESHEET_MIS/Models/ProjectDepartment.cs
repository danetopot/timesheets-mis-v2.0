﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DP_TIMESHEET_MIS.Models
{
    public class ProjectDepartment
    {
        [Key]
        public string Id { get; set; }

        [ForeignKey("ProjectId")]
        public Project Project { get; set; }

        [ForeignKey("DepartmentId")]
        public SystemCodeDetail SystemCodeDetail { get; set; }
    }
}
