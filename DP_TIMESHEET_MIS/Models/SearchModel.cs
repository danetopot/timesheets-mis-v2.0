﻿using PagedList.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Models
{
    public class SearchModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int HoursWorked { get; set; }

        //public IPagedList<SearchModel> Timesheets { get; set; }
    }
}
