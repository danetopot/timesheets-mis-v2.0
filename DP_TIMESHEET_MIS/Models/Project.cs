﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DP_TIMESHEET_MIS.Models
{
    public class Project
    {
        [Key]
        public string Id { get; set; }

        [Required]
        public string RefNo { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Client { get; set; }

        [Required]
        [ForeignKey("TeamLeaderId")]
        public ApplicationUser TeamLeader { get; set; }

        [Required]
        [ForeignKey("ProjectOfficerId")]
        public ApplicationUser ProjectOfficer { get; set; }

        [Required]
        public string Address { get; set; }

        public string Description { get; set; }

        public Country Country { get; set; }

        [ForeignKey("ProjectDepartmentId")]
        public ProjectDepartment ProjectDepartment { get; set; }
    }
}
