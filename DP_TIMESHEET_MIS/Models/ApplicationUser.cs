﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace DP_TIMESHEET_MIS.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string DisplayName => FirstName + " " + Surname;

        public string FileName => FirstName + "_" + Surname;

        public SystemCodeDetail Position { get; set; }

        [ForeignKey("TypeId")]
        public SystemCodeDetail Type { get; set; }

        [ForeignKey("SkillSetId")]
        public SystemCodeDetail SkillSet { get; set; }

        public string JobNo { get; set; }

        public string LineManagerId { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime StartDate { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime EndDate { get; set; }

        public string Signature { get; set; }

        public DateTime? LastPasswordChangedDate { get; set; }

        public DateTime? LastActivityDate { get; set; }

        [DisplayName("Date Created"), DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime DateCreated { get; set; }

        [DisplayName("Created By")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public string CreatedById { get; set; }

        [DisplayName("Created By")]
        public ApplicationUser CreatedBy { get; set; }

        public string MacAddress { get; set; }

        public bool IsLoggedIn { get; set; }
    }
}
