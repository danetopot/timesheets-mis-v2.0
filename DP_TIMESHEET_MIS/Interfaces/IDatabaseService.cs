﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Interfaces
{
    public interface IDatabaseService : IDisposable
    {
        string GetComputerName(string clientIP);

        string GetExternalIP();
    }
}
