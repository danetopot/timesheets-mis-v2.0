﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        /* :::::::: Reading ::::::::*/
        TEntity Find(int? id);

        TEntity Find(string id);

        TEntity SingleOrDefault();

        TEntity First();

        IEnumerable<TEntity> GetAll();

        IEnumerable<TEntity> Where();

        IQueryable<TEntity> FindAll();

        Task<IEnumerable<TEntity>> GetAllAsync();

        /* :::::::: Editing ::::::::*/
        void Add(TEntity entity);

        void AddRange(IEnumerable<TEntity> entities);

        void Update(TEntity entity);

        void Remove(TEntity entity);

        void RemoveRange(IEnumerable<TEntity> entities);

        IQueryable<TEntity> Queryable();
    }
}
