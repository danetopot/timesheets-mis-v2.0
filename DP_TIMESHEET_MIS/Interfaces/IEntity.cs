﻿using System;
using System.ComponentModel.DataAnnotations;
using DP_TIMESHEET_MIS.Models;
using Microsoft.AspNetCore.Mvc;

namespace DP_TIMESHEET_MIS.Interfaces
{
    public interface IEntity
    {
        [HiddenInput]
        object Id { get; set; }

        [Required]
        string CreatedById { get; set; }

        [Required]
        DateTime DateCreated { get; set; }

        [Required]
        ApplicationUser CreatedBy { get; set; }

        string ModifiedById { get; set; }

        DateTime? ModifiedDate { get; set; }

        ApplicationUser ModifiedBy { get; set; }
    }

    public interface IEntity<T> : IEntity
    {
        new T Id { get; set; }
    }
}
