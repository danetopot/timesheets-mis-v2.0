﻿// <auto-generated />
using System;
using DP_TIMESHEET_MIS.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DP_TIMESHEET_MIS.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("CreatedById");

                    b.Property<DateTime>("DateCreated");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<DateTime>("EndDate");

                    b.Property<string>("FirstName");

                    b.Property<bool>("IsLoggedIn");

                    b.Property<string>("JobNo");

                    b.Property<DateTime?>("LastActivityDate");

                    b.Property<DateTime?>("LastPasswordChangedDate");

                    b.Property<string>("LineManagerId");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("MacAddress");

                    b.Property<string>("MiddleName");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<int?>("PositionId");

                    b.Property<string>("SecurityStamp");

                    b.Property<string>("Signature");

                    b.Property<int?>("SkillSetId");

                    b.Property<DateTime>("StartDate");

                    b.Property<string>("Surname");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<int?>("TypeId");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.HasIndex("PositionId");

                    b.HasIndex("SkillSetId");

                    b.HasIndex("TypeId");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.AuditTrail", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ChangeType");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<string>("NewValue");

                    b.Property<string>("OldValue");

                    b.Property<string>("PCName");

                    b.Property<string>("RecordId");

                    b.Property<string>("RequestIpAddress");

                    b.Property<string>("TableName");

                    b.Property<string>("UserAgent");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AuditTrail");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.Country", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Abbreviation")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Country");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.Period", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Active");

                    b.Property<string>("Description");

                    b.Property<string>("Name")
                        .HasMaxLength(20);

                    b.Property<DateTime>("StartDate");

                    b.Property<DateTime>("StopDate");

                    b.HasKey("Id");

                    b.ToTable("Period");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.Project", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired();

                    b.Property<string>("Client")
                        .IsRequired();

                    b.Property<int?>("CountryId");

                    b.Property<string>("Description");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("ProjectDepartmentId");

                    b.Property<string>("ProjectOfficerId")
                        .IsRequired();

                    b.Property<string>("RefNo")
                        .IsRequired();

                    b.Property<string>("TeamLeaderId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.HasIndex("ProjectDepartmentId")
                        .IsUnique()
                        .HasFilter("[ProjectDepartmentId] IS NOT NULL");

                    b.HasIndex("ProjectOfficerId");

                    b.HasIndex("TeamLeaderId");

                    b.ToTable("Project");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.ProjectDepartment", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("DepartmentId");

                    b.Property<string>("ProjectId");

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.HasIndex("ProjectId");

                    b.ToTable("ProjectDepartment");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.ProjectUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ProjectId");

                    b.Property<int?>("RoleId");

                    b.Property<DateTime>("StartDate");

                    b.Property<DateTime>("StopDate");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("ProjectId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("ProjectUser");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.RoleProfile", b =>
                {
                    b.Property<string>("RoleId");

                    b.Property<int>("TaskId");

                    b.HasKey("RoleId", "TaskId");

                    b.HasIndex("TaskId");

                    b.ToTable("RoleProfiles");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.SystemCode", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code");

                    b.Property<string>("Description");

                    b.Property<int?>("SystemModuleId");

                    b.HasKey("Id");

                    b.HasIndex("SystemModuleId");

                    b.ToTable("SystemCode");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.SystemCodeDetail", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedOn");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedOn");

                    b.Property<decimal?>("OrderNo")
                        .IsRequired();

                    b.Property<int>("SystemCodeId");

                    b.HasKey("Id");

                    b.HasIndex("SystemCodeId");

                    b.ToTable("SystemCodeDetails");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.SystemModule", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("SystemModule");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.SystemSetting", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Value")
                        .IsRequired();

                    b.Property<string>("key")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("SystemSetting");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.SystemTask", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.Property<int?>("Order");

                    b.Property<int?>("ParentId");

                    b.HasKey("Id");

                    b.HasIndex("ParentId");

                    b.ToTable("SystemTask");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.TimeSheetDetail", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedById");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime>("DateFilled");

                    b.Property<int>("HoursWorked");

                    b.Property<string>("Location");

                    b.Property<string>("ProjectId");

                    b.Property<int?>("SiteId");

                    b.Property<string>("TaskDescription");

                    b.Property<string>("TimesheetHeaderId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("ProjectId");

                    b.HasIndex("SiteId");

                    b.HasIndex("TimesheetHeaderId");

                    b.ToTable("TimeSheetDetail");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.TimesheetHeader", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApprovedById");

                    b.Property<string>("ConsultantId");

                    b.Property<string>("CreatedById");

                    b.Property<DateTime?>("DateApproved");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateRejected");

                    b.Property<bool>("IsActive");

                    b.Property<int?>("PeriodId");

                    b.Property<string>("ProjectId");

                    b.Property<string>("RejectReason");

                    b.Property<string>("RejectedById");

                    b.Property<string>("TimesheetStatus");

                    b.HasKey("Id");

                    b.HasIndex("ApprovedById");

                    b.HasIndex("ConsultantId");

                    b.HasIndex("CreatedById");

                    b.HasIndex("PeriodId");

                    b.HasIndex("ProjectId");

                    b.HasIndex("RejectedById");

                    b.ToTable("TimesheetHeader");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.ApplicationUser", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.SystemCodeDetail", "Position")
                        .WithMany()
                        .HasForeignKey("PositionId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.SystemCodeDetail", "SkillSet")
                        .WithMany()
                        .HasForeignKey("SkillSetId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.SystemCodeDetail", "Type")
                        .WithMany()
                        .HasForeignKey("TypeId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.AuditTrail", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.Project", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.ProjectDepartment", "ProjectDepartment")
                        .WithOne()
                        .HasForeignKey("DP_TIMESHEET_MIS.Models.Project", "ProjectDepartmentId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser", "ProjectOfficer")
                        .WithMany()
                        .HasForeignKey("ProjectOfficerId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser", "TeamLeader")
                        .WithMany()
                        .HasForeignKey("TeamLeaderId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.ProjectDepartment", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.SystemCodeDetail", "SystemCodeDetail")
                        .WithMany()
                        .HasForeignKey("DepartmentId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.Project", "Project")
                        .WithMany()
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.ProjectUser", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.Project", "Project")
                        .WithMany()
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.SystemCodeDetail", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.RoleProfile", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.SystemTask", "SystemTask")
                        .WithMany()
                        .HasForeignKey("TaskId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.SystemCode", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.SystemModule", "SystemModule")
                        .WithMany()
                        .HasForeignKey("SystemModuleId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.SystemCodeDetail", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.SystemCode", "SystemCode")
                        .WithMany("SystemCodeDetails")
                        .HasForeignKey("SystemCodeId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.SystemTask", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.SystemTask", "Parent")
                        .WithMany("Children")
                        .HasForeignKey("ParentId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.TimeSheetDetail", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.Project", "Project")
                        .WithMany()
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.SystemCodeDetail", "Site")
                        .WithMany()
                        .HasForeignKey("SiteId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.TimesheetHeader", "TimesheetHeader")
                        .WithMany()
                        .HasForeignKey("TimesheetHeaderId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DP_TIMESHEET_MIS.Models.TimesheetHeader", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser", "ApprovedBy")
                        .WithMany()
                        .HasForeignKey("ApprovedById")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser", "Consultant")
                        .WithMany()
                        .HasForeignKey("ConsultantId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.Period", "Period")
                        .WithMany()
                        .HasForeignKey("PeriodId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.Project", "Project")
                        .WithMany()
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser", "RejectedBy")
                        .WithMany()
                        .HasForeignKey("RejectedById")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("DP_TIMESHEET_MIS.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
