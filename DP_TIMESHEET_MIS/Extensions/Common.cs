﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Globalization;
using System.Dynamic;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json;
using PagedList.Core;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Data;
using Syncfusion.EJ2.Linq;

namespace DP_TIMESHEET_MIS.Extensions
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// IQueryableExtension
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        //private static readonly TypeInfo QueryCompilerTypeInfo = typeof(QueryCompiler).GetTypeInfo();
        //private static readonly FieldInfo QueryCompilerField = typeof(EntityQueryProvider).GetTypeInfo().DeclaredFields.First(x => x.Name == "_queryCompiler");
        //private static readonly PropertyInfo NodeTypeProviderField = QueryCompilerTypeInfo.DeclaredProperties.Single(x => x.Name == "NodeTypeProvider");
        //private static readonly MethodInfo CreateQueryParserMethod = QueryCompilerTypeInfo.DeclaredMethods.First(x => x.Name == "CreateQueryParser");
        //private static readonly FieldInfo DataBaseField = QueryCompilerTypeInfo.DeclaredFields.Single(x => x.Name == "_database");
        //private static readonly FieldInfo QueryCompilationContextFactoryField = typeof(Database).GetTypeInfo().DeclaredFields.Single(x => x.Name == "_queryCompilationContextFactory");

        /// <summary>
        /// User ID
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string GetUserId(this ClaimsPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
                return null;

            ClaimsPrincipal currentUser = user;
            return currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
        }

        public static Dictionary<string, string> GetDbStore(ApplicationDbContext context, string userid, int periodid, string projectid)
        {
            try
            {
                IQueryable<TimeSheetDetail> _detail = null;
                string _headerId = string.Empty;
                string _periodStatus = string.Empty;
                int _headerCount = 0;
                int _detailCount = 0;
                int _periodDuration = 0;
                var _appUser = context.Users.Single(i => i.Id == userid);
                var _period = context.Period.Single(i => i.Id == periodid);

                var _header = context.TimesheetHeader.SingleOrDefault(
                    i => i.Consultant.Id == userid &&
                    i.Period.Id == periodid &&
                    i.Project.Id == projectid);

                if (_header != null)
                {
                    _detail = context.TimeSheetDetail.Where(i => i.TimesheetHeader.Id == _header.Id);
                    _detailCount = _detail.Count();
                    _headerId = _header.Id;
                    _headerCount = 1;
                }

                _periodDuration = (int)(
                    Convert.ToDateTime(_period.StopDate) - Convert.ToDateTime(_period.StartDate)
                    ).TotalDays;
                if (_periodDuration == _detailCount) { _periodStatus = "Completed"; }
                else { _periodStatus = "InComplete"; }

                Dictionary<string, string> _userData = new Dictionary<string, string>()
                {
                    { "HeaderID", _headerId.ToString() },
                    { "HeaderCount", _headerCount.ToString() },
                    { "DetailCount", _detailCount.ToString() },
                    { "PeriodDuration", _periodDuration.ToString() },
                    { "PeriodStatus", _periodStatus.ToString() }
                };
                return _userData;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public static async Task<string> GetFileAsync(string path, IFormFile ifile, string email, string type)
        {
            //string path = Path.GetTempPath();

            string _fileUrl = string.Empty;
            string _uploadsDir = string.Empty;

            try
            {
                if (type == "Signature")
                {
                    Directory.CreateDirectory(path + @"\uploads\signatures\");
                    _fileUrl = path + @"\uploads\signatures\" + email + ".txt";
                }
                if (type == "Photo") { _uploadsDir = path + @"\uploads\photo"; }
                if (type == "Audio") { _uploadsDir = path + @"\uploads\audio"; }
                if (type == "Video") { _uploadsDir = path + @"\uploads\video"; }

                await StoreToFileSystemAsync(ifile, _fileUrl);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return _fileUrl;
        }

        public static async Task StoreToFileSystemAsync(IFormFile ifile, string path)
        {
            try
            {
                if (ifile.Length > 0)
                {
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await ifile.CopyToAsync(stream);
                    }
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        public static Chunk RetrieveFromSystemAsync(string path)
        {
            if (File.Exists(path))
            {
                byte[] byteData = System.IO.File.ReadAllBytes(path);
                Byte[] bytes = (Byte[])byteData;
                Image image = Image.GetInstance(bytes);
                Chunk imageChunk = new Chunk(image, 0, 0);
                return imageChunk;
            }
            return null;
        }

        public static List<string> GetTimesheetUserIds(
            ApplicationDbContext context,
            ApplicationUser currentUser,
            IList<string> currentUserRoles)
        {

            string supervisor = "Supervisor";
            string administrator = "Administrator";
            List<string> timesheetuserids = new List<string>();
            if(currentUserRoles.Contains(supervisor)){
                var _lineEmployees = context.Users.Where(i => i.LineManagerId == currentUser.Id);
                if(_lineEmployees!=null)
                {
                    foreach (var _lineEmployee in _lineEmployees)
                    {
                        timesheetuserids.Add(_lineEmployee.Id);
                    }
                }
            }

            if (currentUserRoles.Contains(administrator))
            {
                var _employees = context.Users.ToList();
                if (_employees != null)
                {
                    foreach (var _employee in _employees)
                    {
                        timesheetuserids.Add(_employee.Id);
                    }
                }
            }

            timesheetuserids.Add(currentUser.Id);

            return timesheetuserids;
        }

        public static byte[] GenerateTimesheet(int type, 
            string destPath, 
            IHostingEnvironment _hostingEnvironment, 
            IQueryable<ApplicationUser> user, 
            IQueryable<TimesheetHeader> header, 
            IQueryable<TimeSheetDetail> details)
        {
            byte[] fileBytes = null;
            if (type == 1)
            {
                var _header = header.SingleOrDefault();
                var _linemanagerName = _header.Consultant.DisplayName;
                var _linemanagerPosition = _header.Consultant.Position.Code;
                var _linemanager = user.Where(i => i.Id == _header.Consultant.LineManagerId).FirstOrDefault();
                if (_linemanager!=null) {
                    _linemanagerName = _linemanager.DisplayName;
                    _linemanagerPosition = _linemanager.Position.Code;
                }

                var _consultantSig = RetrieveFromSystemAsync(_header.Consultant.Signature);
                var _supervisorSig = RetrieveFromSystemAsync(_linemanager.Signature);

                var pdfDoc = new Document();
                PdfWriter.GetInstance(pdfDoc, new FileStream(destPath, FileMode.Create));
                pdfDoc.Open();

                // Add Header
                Image img = Image.GetInstance(_hostingEnvironment.WebRootPath + "/images/dp_header.png");
                PdfPCell cell = new PdfPCell();
                cell.Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER | PdfCell.RIGHT_BORDER | PdfCell.LEFT_BORDER;
                cell.AddElement(img);
                PdfPTable table = new PdfPTable(6);
                cell.Colspan = 6;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("PROJECT NAME", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 3;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(_header.Project.Name, new Font(Font.HELVETICA, 8f, Font.BOLD)));
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("PROJECT DP REFERENCE NUMBER", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 3;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(_header.Project.RefNo, new Font(Font.HELVETICA, 8f, Font.BOLD)));
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("CONSULTANT NAME", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 3;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(_header.Consultant.DisplayName, new Font(Font.HELVETICA, 8f, Font.BOLD)));
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("CONSULTANT POSITION", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 3;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(_header.Consultant.Position.Code, new Font(Font.HELVETICA, 8f, Font.BOLD)));
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("PERIOD", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 3;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(_header.Period.Description, new Font(Font.HELVETICA, 8f, Font.BOLD)));
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Date", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Desk Based", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Field Based", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Per Diems(Units)", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Location", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Project Task Description", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);

                int deskdaysworked = 0;
                int fielddaysworked = 0;
                int _deskhoursworked = 0;
                int _fieldhoursworked = 0;
                int _appendrows = 0;
                int _existingrows = 0;
                foreach (var td in details)
                {
                    _existingrows++;
                    int deskhoursworked = 0;
                    int fieldhoursworked = 0;

                    cell = new PdfPCell(new Phrase(td.DateFilled.ToShortDateString(), new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    table.AddCell(cell);


                    if (td.Site.Id == 19)
                    {
                        //deskhoursworked = deskhoursworked + td.HoursWorked;
                        deskhoursworked = td.HoursWorked;
                        _deskhoursworked = _deskhoursworked + deskhoursworked;
                        deskdaysworked++;
                    }
                    if (td.Site.Id == 20)
                    {
                        //fieldhoursworked = fieldhoursworked + td.HoursWorked;
                        fieldhoursworked = td.HoursWorked;
                        _fieldhoursworked = _fieldhoursworked + fieldhoursworked;
                        fielddaysworked++;
                    }

                    cell = new PdfPCell(new Phrase(deskhoursworked.ToString(), new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(fieldhoursworked.ToString(), new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(""));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(td.Location, new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(td.TaskDescription, new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    table.AddCell(cell);
                }

                _appendrows = 31 - _existingrows;
                for (int r = 0; r <= _appendrows; r++)
                {
                    cell = new PdfPCell(new Phrase(" "));
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase(" "));
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase(" "));
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase(" "));
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase(" "));
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase(" "));
                    table.AddCell(cell);
                }

                cell = new PdfPCell(new Phrase("Total Hours", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(_deskhoursworked.ToString(), new Font(Font.HELVETICA, 8f)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(_fieldhoursworked.ToString(), new Font(Font.HELVETICA, 8f)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(""));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(""));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(""));
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Total Days", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(deskdaysworked.ToString(), new Font(Font.HELVETICA, 8f)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(fielddaysworked.ToString(), new Font(Font.HELVETICA, 8f)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(""));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(""));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(""));
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Billable Days", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase((deskdaysworked + fielddaysworked).ToString(), new Font(Font.HELVETICA, 8f)));
                cell.Colspan = 5;
                table.AddCell(cell);
                
                
                cell.Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER | PdfCell.LEFT_BORDER;
                cell = new PdfPCell(new Phrase("Consultant Signature", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                if (_consultantSig != null) { cell = new PdfPCell(new Phrase(_consultantSig)); }
                else { cell = new PdfPCell(new Phrase(" ", new Font(Font.HELVETICA, 8f))); }
                cell.Colspan = 2;
                table.AddCell(cell);

                
                if (_header.TimesheetStatus == "Approved")
                {
                    cell = new PdfPCell(new Phrase("Supervisor Signature", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                    table.AddCell(cell);
                    if (_supervisorSig != null) { cell = new PdfPCell(new Phrase(_supervisorSig)); }
                    else { cell = new PdfPCell(new Phrase("SIGNED", new Font(Font.HELVETICA, 8f))); }
                    cell.Colspan = 3;
                    table.AddCell(cell);
                }
                else
                {
                    cell = new PdfPCell(new Phrase("Supervisor Signature", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase(" ", new Font(Font.HELVETICA, 8f)));
                    cell.Colspan = 3;
                    table.AddCell(cell);
                }
                

                cell = new PdfPCell(new Phrase("Name ", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(_header.Consultant.DisplayName, new Font(Font.HELVETICA, 8f)));
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Name ", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(_linemanagerName, new Font(Font.HELVETICA, 8f)));
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Position ", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(_header.Consultant.Position.Code, new Font(Font.HELVETICA, 8f)));
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Position ", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(_linemanagerPosition, new Font(Font.HELVETICA, 8f)));
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Date", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(DateTime.Now.ToShortDateString(), new Font(Font.HELVETICA, 8f)));
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Date", new Font(Font.HELVETICA, 8f, Font.BOLD)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(DateTime.Now.ToShortDateString(), new Font(Font.HELVETICA, 8f)));
                cell.Colspan = 3;
                table.AddCell(cell);

                pdfDoc.Add(table);
                pdfDoc.Close();

                fileBytes = System.IO.File.ReadAllBytes(destPath);
                return fileBytes;
            }
            return null;
        }

        public static byte[] GenerateProjectReport(int type,
            string destPath,
            IHostingEnvironment _hostingEnvironment,
            IQueryable<Project> project)
        {
            byte[] fileBytes = null;
            var pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            using (FileStream fileStream = File.Open(destPath, FileMode.Create))
            {
                PdfWriter.GetInstance(pdfDoc, fileStream);
                pdfDoc.Open();

                Image img = Image.GetInstance(_hostingEnvironment.WebRootPath + "/images/dp_header.png");
                PdfPCell cell = new PdfPCell();
                cell.Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER | PdfCell.RIGHT_BORDER | PdfCell.LEFT_BORDER;
                cell.AddElement(img);

                PdfPTable table = new PdfPTable(7);
                cell.Colspan = 7;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Reference No.", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 1;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Name", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 1;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Client/Partner", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 1;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Country", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 1;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Team Leader", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 1;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Unit", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 1;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Project Officer", new Font(Font.HELVETICA, 8f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 1;
                table.AddCell(cell);

                foreach (var p in project)
                {
                    cell = new PdfPCell(new Phrase(p.RefNo, new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    cell.Colspan = 1;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(p.Name, new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    cell.Colspan = 1;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(p.Client, new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    cell.Colspan = 1;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(p.Country.Name, new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    cell.Colspan = 1;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(p.TeamLeader.DisplayName, new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    cell.Colspan = 1;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(p.ProjectDepartment.SystemCodeDetail.Code, new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    cell.Colspan = 1;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(p.ProjectOfficer.DisplayName, new Font(Font.HELVETICA, 8f, Font.NORMAL)));
                    cell.Colspan = 1;
                    table.AddCell(cell);
                }


                pdfDoc.Add(table);
                pdfDoc.Close();
            }

            fileBytes = System.IO.File.ReadAllBytes(destPath);
            return fileBytes;
        }

        public static string ToTitleCase(this string title)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title.ToLower());
        }

        public static PagedList<T> Paginate<T>(this IQueryable<T> source,
                                                   int pageIndex, int pageSize)
        {
            return new PagedList<T>(source, pageIndex, pageSize);
        }

        public static ExpandoObject ToExpando(this object anonymousObject)
        {
            IDictionary<string, object> anonymousDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(anonymousObject);
            IDictionary<string, object> expando = new ExpandoObject();
            foreach (var item in anonymousDictionary)
                expando.Add(item);
            return (ExpandoObject)expando;
        }

    }
}

