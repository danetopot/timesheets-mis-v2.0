﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DP_TIMESHEET_MIS.Models;

namespace DP_TIMESHEET_MIS.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public ApplicationDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            base.OnModelCreating(builder);

            builder.Entity<RoleProfile>()
                .HasKey(c => new { c.RoleId, c.TaskId });
        }

        public DbSet<AuditTrail> AuditTrail { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Period> Period { get; set; }
        public DbSet<Project> Project { get; set; }
        public DbSet<ProjectDepartment> ProjectDepartment { get; set; }
        public DbSet<ProjectUser> ProjectUser { get; set; }
        public DbSet<RoleProfile> RoleProfiles { get; set; }
        public DbSet<SystemCode> SystemCode { get; set; }
        public DbSet<SystemCodeDetail> SystemCodeDetails { get; set; }
        public DbSet<SystemModule> SystemModule { get; set; }
        public DbSet<SystemSetting> SystemSetting { get; set; }
        public DbSet<SystemTask> SystemTask { get; set; }
        public DbSet<TimesheetHeader> TimesheetHeader { get; set; }
        public DbSet<TimeSheetDetail> TimeSheetDetail { get; set; }


    }
}
