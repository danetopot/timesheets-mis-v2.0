﻿using System.Threading.Tasks;
using DP_TIMESHEET_MIS.Models;

namespace DP_TIMESHEET_MIS.Services
{
    public interface IDBService
    {
        string GeneratePassword(int maxSize);

        void AuditTrail(AuditTrail auditTrail);

        Task<bool> IsGlobal();
    }
}
