﻿using System;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using DP_TIMESHEET_MIS.Data;
using DP_TIMESHEET_MIS.Models;

namespace DP_TIMESHEET_MIS.Services
{
    public class DBService : IDBService
    {
        private readonly ApplicationDbContext _context;
        private readonly IHttpContextAccessor _http;
        private readonly ApplicationUser _applicationUser;
        private readonly UserManager<ApplicationUser> _userManager;
        public DBService(ApplicationDbContext context, IHttpContextAccessor http,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _http = http;
            _applicationUser = userManager.GetUserAsync(_http.HttpContext.User).Result;
            _userManager = userManager;
        }

        public string GetUserDisplayName()
        {
            return _applicationUser.DisplayName;
        }

        public async Task<bool> IsGlobal()
        {
            return await _userManager.IsInRoleAsync(_applicationUser, "Admin");
        }

        public void AuditTrail(AuditTrail auditTrail)
        {
            if (_applicationUser != null)
                auditTrail.UserId = _applicationUser.Id;
            auditTrail.Date = DateTime.Now;
            auditTrail.UserAgent = _http.HttpContext.Request.Headers["User-Agent"];
            auditTrail.RequestIpAddress = _http.HttpContext.Connection.RemoteIpAddress.ToString();
            _context.AuditTrail.Add(auditTrail);
        }

        public string GeneratePassword(int maxSize)
        {
            var passwords = string.Empty;
            var chArray1 = new char[52];
            var chArray2 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^*()_+".ToCharArray();
            var data1 = new byte[1];
            using (var cryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                cryptoServiceProvider.GetNonZeroBytes(data1);
                var data2 = new byte[maxSize];
                cryptoServiceProvider.GetNonZeroBytes(data2);
                var stringBuilder = new StringBuilder(maxSize);
                foreach (var num in data2)
                    stringBuilder.Append(chArray2[(int)num % chArray2.Length]);
                passwords = stringBuilder.ToString();
                var number = Random("N");
                var upper = Random("S");
                var lower = Random("l");
                passwords += number + upper;
                return passwords;
            }
        }
        public string Random(string type)
        {
            var data2 = new byte[1];
            var passwords = string.Empty;
            switch (type)
            {
                case "N":
                    {
                        var charArray = "0123456789";
                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                            stringBuilder.Append(charArray[(int)num % charArray.Length]);
                        passwords = stringBuilder.ToString();
                        return passwords;
                    }

                case "l":
                    {
                        var charArray = "abcdefghijklmnopqrstuvwxyz";

                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                            stringBuilder.Append(charArray[(int)num % charArray.Length]);
                        passwords = stringBuilder.ToString();
                        return passwords;
                    }

                case "C":
                    {
                        var charArray = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                            stringBuilder.Append(charArray[(int)num % charArray.Length]);
                        passwords = stringBuilder.ToString();
                        return passwords;
                    }

                case "S":
                    {
                        var charArray = "!@#$%^&*()_+-={}|[]:;<>?,./";
                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                            stringBuilder.Append(charArray[(int)num % charArray.Length]);
                        passwords = stringBuilder.ToString();
                        return passwords;
                    }
            }

            return string.Empty;
        }
    }
}
