﻿using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Services
{
    public interface ILogService
    {
        void DBLog(string type, string exc, string category, string item, string UserId);

        void FileLog(string data);

        void LogWrite(string type, string exc, string category, string item, string UserId);

        // string GetErrorListFromModelState(ModelStateDictionary modelState);
    }
}
