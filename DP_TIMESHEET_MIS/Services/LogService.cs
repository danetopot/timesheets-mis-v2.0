﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DP_TIMESHEET_MIS.Services
{
    public class LogService : ILogService
    {
        private string path = "";
        private readonly IHostingEnvironment _hostingEnvironment;


        public LogService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            string webRootPath = _hostingEnvironment.WebRootPath;
            path = webRootPath + "/logs/";
        }

        public void DBLog(string type, string exc, string category, string item, string UserId)
        {

        }

        public void FileLog(string data)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }

            t = dt.ToString("T");
            var fs = new FileStream(
                $"{this.path}log{todaydate}.txt",
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine("****************************************************************************************************************************");
                mStreamWriter.WriteLine("Date: {0}   Time : {1}\n", todaydate, t);
                mStreamWriter.WriteLine(data);
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }
        public void LogWrite(string type, string exc, string category, string item, string UserId)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }

            t = dt.ToString("T");
            var fs = new FileStream(
                $"{this.path}-{todaydate}-{hour}-00.txt",
                FileMode.OpenOrCreate,
                FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);

                mStreamWriter.WriteLine("");
                mStreamWriter.WriteLine(exc);

                mStreamWriter.WriteLine("");
                mStreamWriter.WriteLine(exc);

                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }
        public void MailLog(string type, string exc, string category, string item, string UserId)
        {
        }
    }
}
