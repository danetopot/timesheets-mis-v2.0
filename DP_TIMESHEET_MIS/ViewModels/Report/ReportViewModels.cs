﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using PagedList.Core;
using DP_TIMESHEET_MIS.ViewModels.Base;
using DP_TIMESHEET_MIS.Models;

namespace DP_TIMESHEET_MIS.ViewModels.Report
{
    public class ReportViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "Report Type is required")]
        public int? ReportType { get; set; }

        //[Required]
        //[Display(Name = "Department")]
        //public string Consultant { get; set; }

        //[Required]
        //[Display(Name = "Department")]
        //public int Department { get; set; }

        //[Required]
        //[Display(Name = "Project")]
        //public int? Project { get; set; }

        //[Required]
        //[Display(Name = "Period")]
        //public int? Period { get; set; }
    }
}
