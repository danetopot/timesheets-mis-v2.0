﻿using DP_TIMESHEET_MIS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.ViewModels.Dashboard
{
    public class DashboardViewModel
    {
        public List<string> Labels { get; set; }
        public String Label { get; set; }
        public List<string> BackgroundColors { get; set; }
        public List<decimal> HoursWorked { get; set; }
        public decimal YaxisMax { get; set; }

        public int? Period { get; set; }

        public int TotalIncomplete { get; set; }
        public int TotalSubmitted { get; set; }
        public int TotalPending { get; set; }
        public int TotalRejected { get; set; }
        public int TotalApproved { get; set; }
    }
}