﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using PagedList.Core;
using DP_TIMESHEET_MIS.ViewModels.Base;
using DP_TIMESHEET_MIS.Models;

namespace DP_TIMESHEET_MIS.ViewModels.Administration
{
    public class NewProfileViewModel
    {
        [Required]
        public string Name { get; set; }

        //[Required]
        //public string UserId { get; set; }
    }

    public class NewProjectViewModel
    {

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public string RefNo { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Client { get; set; }

        [Required]
        public string TeamLeader { get; set; }

        [Required]
        public string ProjectOfficer { get; set; }

        public string Description { get; set; }

        [Required]
        public int Department { get; set; }

        [Required]
        public int Country { get; set; }
    }

    public class EditProjectViewModel
    {
        [Required]
        public string ProjectId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public string RefNo { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Client { get; set; }

        [Required]
        public string TeamLeader { get; set; }

        [Required]
        public string ProjectOfficer { get; set; }

        public string Description { get; set; }

        [Required]
        public int Department { get; set; }

        [Required]
        public int Country { get; set; }
    }

    public class SearchProjectViewModel : BaseViewModel
    {
        public string Name { get; set; }

        public IPagedList<Project> Projects { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }

    public class NewProjectAssignmentViewModel
    {
        [Required]
        public int Department { get; set; }

        [Required]
        [StringLength(100)]
        public string Consultant { get; set; }

        [Required]
        public string Project { get; set; }

        [Required]
        public int Role { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("Stop Date")]
        public DateTime? StopDate { get; set; }
    }

    public class EditProjectAssignmentViewModel
    {
        [Required]
        public string ProjectAssignmentId { get; set; }

        [Required]
        public int Department { get; set; }

        [Required]
        [StringLength(100)]
        public string Consultant { get; set; }

        [Required]
        public string Project { get; set; }

        [Required]
        public int Role { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("Stop Date")]
        public DateTime? StopDate { get; set; }
    }

    public class SearchProjectAssignmentViewModel : BaseViewModel
    {
        public string Name { get; set; }

        public string Project { get; set; }

        public IPagedList<ProjectUser> ProjectUser { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }
    public class NewPeriodViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("Stop Date")]
        public DateTime? StopDate { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public bool Active { get; set; }
    }

    public class EditPeriodViewModel
    {
        [Required]
        public int PeriodId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("Stop Date")]
        public DateTime? StopDate { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public bool Active { get; set; }
    }

    public class SearchPeriodViewModel : BaseViewModel
    {
        public string Name { get; set; }

        public string Project { get; set; }

        public IPagedList<Period> Period { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }

    public class NewUserViewModel : BaseViewModel
    {
        [Required]
        [Display(Name = "First Name")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "SurName")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string SurName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone number must be numeric")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Position")]
        public int? Position { get; set; }

        [Required]
        [Display(Name = "Job No")]
        public string JobNo { get; set; }

        [Required]
        [Display(Name = "Type")]
        public int? Type { get; set; }

        [Required]
        [Display(Name = "SkillSet")]
        public int? SkillSet { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [Required]
        public string LineManager { get; set; }

        public string MsgType { get; set; }
    }

    public class EditUserViewModel : BaseViewModel
    {
        public string UserId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "SurName")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string SurName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone number must be numeric")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Position")]
        public int? Position { get; set; }

        [Required]
        [Display(Name = "Job No")]
        public string JobNo { get; set; }

        [Required]
        [Display(Name = "Type")]
        public int? Type { get; set; }

        [Required]
        [Display(Name = "SkillSet")]
        public int? SkillSet { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [Required]
        public string LineManager { get; set; }

        public string MsgType { get; set; }
    }

    public class ViewUserViewModel : BaseViewModel
    {
        [Required]
        [Display(Name = "First Name")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "SurName")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string SurName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone number must be numeric")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Position")]
        public int? Position { get; set; }

        [Required]
        [Display(Name = "Job No")]
        public string JobNo { get; set; }

        [Required]
        [Display(Name = "Type")]
        public int? Type { get; set; }

        [Required]
        [Display(Name = "SkillSet")]
        public int? SkillSet { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [Required]
        public string LineManager { get; set; }

        public string MsgType { get; set; }
    }

    public class SearchUserViewModel : BaseViewModel
    {
        public string Email { get; set; }

        public IPagedList<ApplicationUser> Users { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }

    public class ManageUserViewModel : BaseViewModel
    {
        public string UserId { get; set; }

        public string UserProfile { get; set; }

        public string DisplayName { get; set; }

        public string Email { get; set; }

        // public ICollection<string> Profiles { get; set; }
        public string Profiles { get; set; }

        public IPagedList<ApplicationUser> Users { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }

    public class ManageProfileViewModel : BaseViewModel
    {
        public ICollection<SystemTask> SystemTasks { get; set; }

        public int[] Ids { get; set; }

        public string RoleId { get; set; }

        public ICollection<int> RoleProfileIds { get; set; }
    }

    public class UserRolesViewModel
    {
        public string Text { get; set; }

        // Opened/Selected
        public Dictionary<string, bool> State { get; set; }

        public IList<Dictionary<string, string>> Children { get; set; }
    }
}
