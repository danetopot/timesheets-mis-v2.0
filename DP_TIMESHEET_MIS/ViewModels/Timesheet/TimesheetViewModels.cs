﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Configuration;
using PagedList.Core;
using DP_TIMESHEET_MIS.ViewModels.Base;
using DP_TIMESHEET_MIS.Models;
using System.Linq;

namespace DP_TIMESHEET_MIS.ViewModels.Timesheet
{
    public class SignatureViewModel
    {
        [Required]
        [Display(Name = "Signature")]
        [FileExtensions(Extensions = "jpg,png,jpeg,bmp,svg")]
        public IFormFile Signature { get; set; }
    }

    public class SearchTableViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int HoursWorked { get; set; }

        public IPagedList<SearchModel> Timesheets { get; set; }

        public int Period { get; set; }

        public string PeriodName { get; set; }

        public string HeaderId { get; set; }

        public int Department { get; set; }

        public string Project { get; set; }

        public string ProjectId { get; set; }

        public string SearchName { get; set; }

        public string TimesheetStatus { get; set; }

        public ApplicationUser ApprovedBy { get; set; }

        public string CreatedBy { get; set; }

        public bool isApprover { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }

    public class SearchTimesheetViewModel : BaseViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int HoursWorked { get; set; }

        public IPagedList<SearchModel> Timesheets { get; set; }

        public int Period { get; set; }

        public string HeaderId { get; set; }

        public int Department { get; set; }

        public string Project { get; set; }

        public string SearchName { get; set; }

        //public IEnumerable<dynamic> Timesheet { get; set; }
        public IPagedList<SearchTableViewModel> Timesheet { get; set; }

        public string TimesheetStatus { get; set; }

        public ApplicationUser ApprovedBy { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }

        //public bool IsApprover { get; set; }
    }

    public class NewTimesheetViewModel : BaseViewModel
    {
        [Required]
        [Display(Name = "Period")]
        public int? Period { get; set; }

        //[Required]
        //[Display(Name = "Role")]
        //public int? Role { get; set; }

        [Required]
        [Display(Name = "Site")]
        public int? Site { get; set; }

        [Required]
        [Display(Name = "Project")]
        public string Project { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("DateFilled")]
        public DateTime? DateFilled { get; set; }

        [Required]
        [Range(1, 24)]
        public int HoursWorked { get; set; }

        [Required]
        public string Location { get; set; }

        [DataType(DataType.MultilineText)]
        public string TaskDescription { get; set; }
    }

    public class ViewTimesheetViewModel : BaseViewModel
    {
        public string HeaderId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Department { get; set; }

        public string Project { get; set; }

        public IEnumerable<string> Projects { get; set; }

        public IPagedList<TimeSheetDetail> Timesheets { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }

    public class EditTimesheetViewModel : BaseViewModel
    {
        public string Id { get; set; }
        public string HeaderId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Department { get; set; }

        public string Project { get; set; }

        public string Period { get; set; }

        public IEnumerable<string> Projects { get; set; }

        public IPagedList<TimeSheetDetail> Timesheets { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }

    public class EditTimesheetDetailViewModel : BaseViewModel
    {
        public string Consultant { get; set; }

        public string DetailId { get; set; }

        public string HeaderId { get; set; }

        [Required]
        [Display(Name = "Period")]
        public int? Period { get; set; }

        [Required]
        [Display(Name = "Site")]
        public int? Site { get; set; }

        [Required]
        [Display(Name = "Project")]
        public string Project { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}"), DataType(DataType.Text)]
        [DisplayName("DateFilled")]
        public DateTime? DateFilled { get; set; }

        [Required]
        [Range(1, 24)]
        public int HoursWorked { get; set; }

        [Required]
        public string Location { get; set; }

        public string TaskDescription { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }

    public class ApproveTimesheetViewModel : BaseViewModel
    {
        public string HeaderId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Department { get; set; }

        public string Project { get; set; }

        public string Period { get; set; }

        public string RejectReason { get; set; }

        public IEnumerable<string> Projects { get; set; }

        public IPagedList<TimeSheetDetail> Timesheets { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }

    public class SendToApprovalTimesheetViewModel : BaseViewModel
    {
        public string HeaderId { get; set; }

        public IPagedList<TimeSheetDetail> Timesheets { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }
}
