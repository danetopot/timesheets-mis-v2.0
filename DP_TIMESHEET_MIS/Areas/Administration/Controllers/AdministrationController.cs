﻿using System.Threading.Tasks;
using System.Linq;
using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using PagedList.Core;
using Syncfusion.EJ2.Linq;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.ViewModels.Administration;
using DP_TIMESHEET_MIS.Services;
//using DP_TIMESHEET_MIS.Services.Email;
using DP_TIMESHEET_MIS.Data;
using DP_TIMESHEET_MIS.Extensions;
using DP_TIMESHEET_MIS.Interfaces;
using static DP_TIMESHEET_MIS.Extensions.ExtensionMethods;
using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace DP_TIMESHEET_MIS.Areas.Administration.Controllers
{
    [Area("Administration")]
    public class AdministrationController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;
        private readonly IDBService _dbService;
        private readonly IEmailService _emailService;
        private readonly IUnitOfWork _uow;


        public AdministrationController(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            IDBService dbService,
            IEmailService emailService,
            IUnitOfWork uow,
            ILogger<AdministrationController> logger)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
            _dbService = dbService;
            _emailService = emailService;
            _uow = uow; ;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [Authorize]
        [HttpGet]
        [Route("Administration/Security/SearchUser")]
        [Permission("Manage Users:Create")]
        public IActionResult SearchUser(SearchUserViewModel vm, string id, string returnUrl = null)
        {
            var userId = User.GetUserId();
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;

            var user = _context.Users.
                OrderByDescending(c => c.DateCreated)
               .AsQueryable();

            if (id != null)
            {
                user = user.Where(i => i.Id == id);
            }

            vm.Users = user.ToPagedList(page, pageSize);
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Security/SearchUser")]
        public IActionResult SearchUser(SearchUserViewModel vm, string returnUrl = null)
        {
            var userId = User.GetUserId();
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;

            var user = _context.Users.
                OrderByDescending(c => c.DateCreated)
               .AsQueryable();

            if (!string.IsNullOrEmpty(vm.Email))
            {
                user = user.Where(
                    u => u.Email.Contains(vm.Email)
                    );
            }

            vm.Users = user.ToPagedList(page, pageSize);
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/Security/ManageUser")]
        [Permission("Manage Users:Manage Role")]
        public IActionResult ManageUser(string id, string returnUrl = null)
        {
            var vm = new ManageUserViewModel();
            var _consultant = _context.Users.Single(u => u.Id == id);
            vm.UserId = id;
            vm.DisplayName = _consultant.DisplayName;
            // var _existingprofiles = _context.UserRoles.Where(u => u.UserId == vm.UserId).ToList();
            ViewBag.UserProfile = new SelectList(_context.Roles, "Id", "Name");
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Security/ManageUser")]
        public async Task<IActionResult> ManageUser(ManageUserViewModel vm, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                IList<string> _existingrpflist = new List<string>();
                var _newrole = vm.Profiles;
                var _user = _context.Users.Single(u => u.Id == vm.UserId);
                var role = _context.Roles.Single(i => i.Id == _newrole);

                if (!await _userManager.IsInRoleAsync(_user, role.Name))
                {
                    TempData["Success"] = "Role/Profile Saved Successfully";
                    return RedirectToAction(nameof(SearchUser));
                }
                else
                {
                    await _userManager.AddToRoleAsync(_user, role.Name);
                }
                vm.DisplayName = _user.DisplayName.ToTitleCase();
            }
            TempData["Success"] = "Role/Profile Saved Successfully";
            return RedirectToAction(nameof(SearchUser));
        }

        [Authorize]
        [Route("Administration/Security/ManageProfile/{Id?}")]
        [Permission("Manage Users:Manage Role")]
        public IActionResult ManageProfile(string id)
        {
            var rolesArray = _context.Roles.OrderBy(r => r.Name).ToList();
            var vm = new ManageProfileViewModel();
            id = string.IsNullOrEmpty(id) ? rolesArray.First().Id : id;
            ViewBag.RoleId = new SelectList(rolesArray, "Id", "Name", id);
            vm.SystemTasks = _context.SystemTask
                .Include(i => i.Children)
                .OrderBy(i => i.Id)
                .Where(t => t.ParentId == null).ToList();
            vm.RoleProfileIds = _context.RoleProfiles.Where(r => r.RoleId == id).Select(r => r.TaskId).ToList();
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/Security/ManageProfile/New")]
        [Permission("Manage Users:Create")]
        public IActionResult NewProfile(string returnUrl = null)
        {
            NewProfileViewModel vm = new NewProfileViewModel();
            //vm.UserId = id;
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Security/ManageProfile/New")]
        [Permission("Manage Users:Create")]
        public async Task<IActionResult> NewProfile(NewProfileViewModel vm, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var roleExist = await _roleManager.RoleExistsAsync(vm.Name);
                if (!roleExist)
                {
                    var identityrole = new IdentityRole();
                    identityrole.Name = vm.Name;
                    await _roleManager.CreateAsync(identityrole);

                    TempData["Success"] = "Profile Saved Successfully";
                    // return RedirectToAction(nameof(ManageProfile), new { id=vm.UserId});
                    return RedirectToAction(nameof(ManageProfile));
                }
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Security/ManageProfile/{Id?}")]
        public IActionResult ManageProfile(ManageProfileViewModel vm)
        {
            _context.RoleProfiles.RemoveRange(
                _context.RoleProfiles.Where(d => d.RoleId == vm.RoleId));
            _context.SaveChanges();
            foreach (var taskId in vm.Ids)
            {
                var roleProfile = new RoleProfile
                {
                    TaskId = taskId,
                    RoleId = vm.RoleId
                };
                _context.RoleProfiles.Add(roleProfile);
            }
            _context.SaveChanges();

            var rolesArray = _context.Roles.ToList();
            ViewBag.RoleId = new SelectList(rolesArray, "Id", "Name");
            var model = new ManageProfileViewModel();
            string id = vm.RoleId;
            vm.SystemTasks = _context.SystemTask.Include(i => i.Children).OrderBy(i => i.Order)
                .Where(t => t.ParentId == null).ToList();
            vm.RoleProfileIds = _context.RoleProfiles.Where(r => r.RoleId == id).Select(r => r.TaskId).ToList();
            TempData["SuccessMsg"] = "Profile Updated Successfully";
            return RedirectToAction(nameof(ManageProfile));
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/Security/NewUser")]
        [Permission("Manage Users:Create")]
        public IActionResult NewUser(string id, string returnUrl = null)
        {
            var _lineManagerList = _context.Users.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });

            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.LineManagerTypeId = new SelectList(_lineManagerList, "Value", "Text");
            ViewBag.UserTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Internal External Categories"), "Id", "Code");
            ViewBag.SkillSetTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "SkillSet Categories"), "Id", "Code");
            ViewBag.PositionTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Consultant Position Categories"), "Id", "Code");
            return View();
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Security/NewUser")]
        public async Task<IActionResult> NewUser(NewUserViewModel vm, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser();
                new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, user);
                user.UserName = vm.Email;
                user.Position = _context.SystemCodeDetails.Single(i => i.Id == vm.Position);
                user.Type = _context.SystemCodeDetails.Single(i => i.Id == vm.Type);
                user.SkillSet = _context.SystemCodeDetails.Single(i => i.Id == vm.SkillSet);
                user.LineManagerId = vm.LineManager;
                user.CreatedBy = _context.Users.Single(i => i.Id == User.GetUserId());
                user.DateCreated = DateTime.Now;
                if (_context.Users.SingleOrDefault(i => i.Email == vm.Email) != null)
                {
                    TempData["Error"] = "User with that email address exists!";
                    return RedirectToAction(nameof(SearchUser), new { id = "" });
                }
                _context.Add(user);
                await _context.SaveChangesAsync();

                // Generate Random Password
                var password = _dbService.GeneratePassword(6);
                var result = await _userManager.AddPasswordAsync(user, password);
                if (result.Succeeded)
                {
                    string message = String.Format("Dear {0}, your password to login in to Timesheet Portal is {1} \nReset on first login.", vm.FirstName, password);
                    string subject = String.Format("{0}", "One-Time Login Password To Timesheet App");
                    await _emailService.SendEmailAsync(vm.Email, subject, message);
                    _logger.LogInformation("User created and password sent");
                }
                TempData["Success"] = "User Saved and Password Sent Successfully!";
                return RedirectToAction(nameof(SearchUser), new { id = "" });
            }

            var _lineManagerList = _context.Users.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });

            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.LineManagerTypeId = new SelectList(_lineManagerList, "Value", "Text");
            ViewBag.UserTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Internal External Categories"), "Id", "Code");
            ViewBag.SkillSetTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "SkillSet Categories"), "Id", "Code");
            ViewBag.PositionTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Consultant Position Categories"), "Id", "Code");
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/Security/EditUser")]
        [Permission("Manage Users:Create")]
        public IActionResult EditUser(string id, string returnUrl = null)
        {
            var user = _context.Users
                .Include(p => p.Position.SystemCode)
                .Include(t => t.Type)
                .Include(s => s.SkillSet.SystemCode)
                .Single(i => i.Id == id);

            EditUserViewModel vm = new EditUserViewModel();
            new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(user, vm);
            vm.UserId = id;
            vm.Position = user.Position.Id;
            vm.LineManager = user.LineManagerId;
            vm.Type = user.Type.Id;
            vm.SkillSet = user.SkillSet.Id;

            var _lineManagerList = _context.Users.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });

            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.LineManagerTypeId = new SelectList(_lineManagerList, "Value", "Text");
            ViewBag.UserTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Internal External Categories"), "Id", "Code");
            ViewBag.SkillSetTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "SkillSet Categories"), "Id", "Code");
            ViewBag.PositionTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Consultant Position Categories"), "Id", "Code");
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Security/EditUser")]
        [Permission("Manage Users:Create")]
        public IActionResult EditUser(EditUserViewModel vm, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var user = _context.Users
                .Include(p => p.Position.SystemCode)
                .Include(t => t.Type)
                .Include(s => s.SkillSet.SystemCode)
                .Single(i => i.Id == vm.UserId);

                new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, user);
                user.Position = _context.SystemCodeDetails.Single(i => i.Id == vm.Position);
                user.LineManagerId = vm.LineManager;
                user.Type = _context.SystemCodeDetails.Single(i => i.Id == vm.Type);
                user.SkillSet = _context.SystemCodeDetails.Single(i => i.Id == vm.SkillSet);
                _uow.GetRepository<ApplicationUser>().Update(user);
                _context.SaveChanges();

                TempData["Success"] = "User Updated Successfully!";
                return RedirectToAction(nameof(SearchUser), new { id = vm.UserId });
            }

            var _lineManagerList = _context.Users.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });

            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.LineManagerTypeId = new SelectList(_lineManagerList, "Value", "Text");
            ViewBag.UserTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Internal External Categories"), "Id", "Code");
            ViewBag.SkillSetTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "SkillSet Categories"), "Id", "Code");
            ViewBag.PositionTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Consultant Position Categories"), "Id", "Code");
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/Project/Search")]
        [Permission("Settings:Create")]
        public IActionResult SearchProject(string id, string returnUrl = null)
        {
            SearchProjectViewModel vm = new SearchProjectViewModel();
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;
            var projects = _context.Project
                .Include(i => i.ProjectDepartment.SystemCodeDetail)
                .Include(i => i.Country)
                .OrderByDescending(c => c.Name)
               .AsQueryable();

            if (id != null)
            {
                projects = projects.Where(i => i.Id == id);
            }
            //var test = projects.ToList();
            vm.Projects = projects.ToPagedList(page, pageSize);
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Project/Search")]
        [Permission("Settings:Create")]
        public IActionResult SearchProject(SearchProjectViewModel vm, string returnUrl = null)
        {
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;
            var projects = _context.Project
                .Include(i => i.ProjectDepartment.SystemCodeDetail)
                .Include(i => i.Country)
                .OrderByDescending(c => c.Name)
               .AsQueryable();

            if (!string.IsNullOrEmpty(vm.Name))
            {
                projects = projects.Where(i => i.Name.Contains(vm.Name, StringComparison.OrdinalIgnoreCase) || 
                i.RefNo.Contains(vm.Name, StringComparison.OrdinalIgnoreCase));
            }

            vm.Projects = projects.ToPagedList(page, pageSize);
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/Project/New")]
        [Permission("Settings:Create")]
        public IActionResult NewProject(string returnUrl = null)
        {
            var pteams = _context.Users.Include(s => s.SkillSet.SystemCode.SystemCodeDetails);
            var pteam = pteams.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });
            var tteam = pteams.Where(i => i.SkillSet.Code == "Technical").Select(s => new SelectListItem
                {
                    Value = s.Id.ToString(),
                    Text = s.DisplayName
                });

            ViewBag.TechnicalTeamTypeId = new SelectList(tteam, "Value", "Text");
            ViewBag.ProjectTeamTypeId = new SelectList(pteam, "Value", "Text");
            ViewBag.CountryTypeId = new SelectList(_context.Country, "Id", "Name");
            ViewBag.DepartmentTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Department Categories"), "Id", "Code");
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Project/New")]
        [Permission("Settings:Create")]
        public async Task<IActionResult> NewProject(NewProjectViewModel vm, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var _departments = vm.Department;
                Project project = new Project();
                new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, project);
                project.Id = Guid.NewGuid().ToString().ToLower();
                project.Country = _context.Country.Single(i => i.Id == vm.Country);
                project.TeamLeader = _context.Users.Single(i => i.Id == vm.TeamLeader);
                project.ProjectOfficer = _context.Users.Single(i=>i.Id==vm.ProjectOfficer);
                _context.Add(project);
                await _context.SaveChangesAsync();

                var pd = new ProjectDepartment
                {
                    Id = Guid.NewGuid().ToString().ToLower(),
                    Project = _context.Project.Single(i => i.Id == project.Id),
                    SystemCodeDetail = _context.SystemCodeDetails.Single(u => u.Id == vm.Department)
                };
                _context.ProjectDepartment.Add(pd);
                await _context.SaveChangesAsync();

                project.ProjectDepartment = _context.ProjectDepartment.Find(pd.Id);
                _uow.GetRepository<Project>().Update(project);
                await _context.SaveChangesAsync();

                TempData["Success"] = "Project Saved Successfully!";
                return RedirectToAction(nameof(SearchProject), new { id = project.Id });
            }

            var pteams = _context.Users.Include(s => s.SkillSet.SystemCode.SystemCodeDetails);
            var pteam = pteams.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });
            var tteam = pteams.Where(i => i.SkillSet.Code == "Technical").Select(s => new SelectListItem
                {
                    Value = s.Id.ToString(),
                    Text = s.DisplayName
                });

            ViewBag.TechnicalTeamTypeId = new SelectList(tteam, "Value", "Text");
            ViewBag.ProjectTeamTypeId = new SelectList(pteam, "Value", "Text");
            ViewBag.CountryTypeId = new SelectList(_context.Country, "Id", "Name");
            ViewBag.DepartmentTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Department Categories"), "Id", "Code");
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/Project/Edit")]
        [Permission("Settings:Create")]
        public IActionResult EditProject(string id, string returnUrl = null)
        {
            var project = _context.Project
                .Include(i=>i.ProjectDepartment)
                .Include(i => i.ProjectDepartment.SystemCodeDetail)
                .Include(i => i.Country)
                .Include(i => i.TeamLeader)
                .Include(i => i.ProjectOfficer)
                .Single(p=>p.Id==id);
            EditProjectViewModel vm = new EditProjectViewModel();
            new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(project, vm);
            vm.ProjectId = id;
            vm.Department = project.ProjectDepartment.SystemCodeDetail.Id;
            vm.Country = project.Country.Id;
            vm.TeamLeader = project.TeamLeader.Id;
            vm.ProjectOfficer = project.ProjectOfficer.Id;

            var pteams = _context.Users.Include(s => s.SkillSet.SystemCode.SystemCodeDetails);
            var pteam = pteams.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });
            var tteam = pteams.Where(i => i.SkillSet.Code == "Technical").Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });

            ViewBag.TechnicalTeamTypeId = new SelectList(tteam, "Value", "Text");
            ViewBag.ProjectTeamTypeId = new SelectList(pteam, "Value", "Text");
            ViewBag.CountryTypeId = new SelectList(_context.Country, "Id", "Name");
            ViewBag.DepartmentTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Department Categories"), "Id", "Code");
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Project/Edit")]
        [Permission("Settings:Create")]
        public IActionResult EditProject(EditProjectViewModel vm, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var project = _context.Project
                    .Include(i => i.ProjectDepartment)
                    .Include(i => i.ProjectDepartment.SystemCodeDetail)
                    .Include(i => i.Country)
                    .Include(i => i.TeamLeader)
                    .Include(i => i.ProjectOfficer)
                    .Single(p => p.Id == vm.ProjectId);

                new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, project);
                project.Country = _context.Country.Single(i => i.Id == vm.Country);
                project.TeamLeader = _context.Users.Single(i => i.Id == vm.TeamLeader);
                project.ProjectOfficer = _context.Users.Single(i => i.Id == vm.ProjectOfficer);
                _uow.GetRepository<Project>().Update(project);
                _context.SaveChanges();

                var projectdepartment = _context.ProjectDepartment
                    .Single(d => d.Project.Id == vm.ProjectId);
                projectdepartment.SystemCodeDetail = _context.SystemCodeDetails.Single(i => i.Id == vm.Department);
                _uow.GetRepository<ProjectDepartment>().Update(projectdepartment);
                _context.SaveChanges();

                TempData["Success"] = "Project Updated Successfully!";
                return RedirectToAction(nameof(SearchProject), new { id = project.Id });
            }

            var pteams = _context.Users.Include(s => s.SkillSet.SystemCode.SystemCodeDetails);
            var pteam = pteams.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });
            var tteam = pteams.Where(i => i.SkillSet.Code == "Technical").Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });

            ViewBag.TechnicalTeamTypeId = new SelectList(tteam, "Value", "Text");
            ViewBag.ProjectTeamTypeId = new SelectList(pteam, "Value", "Text");
            ViewBag.CountryTypeId = new SelectList(_context.Country, "Id", "Name");
            ViewBag.DepartmentTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Department Categories"), "Id", "Code");
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/ProjectAssignment/Search")]
        [Permission("Settings:Create")]
        public IActionResult SearchProjectAssignment(string id, string returnUrl = null)
        {
            SearchProjectAssignmentViewModel vm = new SearchProjectAssignmentViewModel();
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;
            var projectassignments = _context.ProjectUser
                .Include(u => u.User)
                .Include(s=> s.Role)
                //.OrderByDescending(c => c.User.FirstName)
               .AsQueryable();

            
            vm.ProjectUser = projectassignments.ToPagedList(page, pageSize); ;
            ViewBag.ProjectTypeId = new SelectList(_context.Project, "Id", "Name");
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/ProjectAssignment/Search")]
        [Permission("Settings:Create")]
        public IActionResult SearchProjectAssignment(SearchProjectAssignmentViewModel vm, string id, string returnUrl = null)
        {
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;
            var projectassignments = _context.ProjectUser
                .Include(p=>p.Project)
                .Include(u => u.User)
                .Include(s => s.Role)
               //.OrderByDescending(c => c.User.FirstName)
               .AsQueryable();

            if (!string.IsNullOrEmpty(vm.Name))
            {
                projectassignments = projectassignments.Where(i => i.User.FirstName.Contains(vm.Name, StringComparison.OrdinalIgnoreCase) ||
                i.User.Surname.Contains(vm.Name, StringComparison.OrdinalIgnoreCase));
            }

            if (!string.IsNullOrEmpty(vm.Project))
            {
                projectassignments = projectassignments.Where(i => i.Project.Id ==vm.Project);
            }

            vm.ProjectUser = projectassignments.ToPagedList(page, pageSize); ;
            ViewBag.ProjectTypeId = new SelectList(_context.Project, "Id", "Name");
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/ProjectAssignment/New")]
        [Permission("Settings:Create")]
        public IActionResult NewProjectAssignment(string returnUrl = null)
        {
            var _selectList = _context.Users.Select(s => new SelectListItem {
                    Value = s.Id.ToString(),
                    Text = s.DisplayName
                });

            ViewBag.DepartmentTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Department Categories"), "Id", "Code");
            ViewBag.ConsultantTypeId = new SelectList(_selectList, "Value", "Text");
            ViewBag.RoleTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Project Role Categories"), "Id", "Code");

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/ProjectAssignment/New")]
        [Permission("Settings:Create")]
        public async Task<IActionResult> NewProjectAssignment(NewProjectAssignmentViewModel vm, string returnUrl = null)
        {
            var _selectList = _context.Users.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });

            if (ModelState.IsValid)
            {
                var exists = _context.ProjectUser.Where(i => i.User.Id == vm.Consultant && i.Role.Id == vm.Role && i.Project.Id == vm.Project);
                if (exists.Any())
                {
                    TempData["SuccessMsg"] = "User/Project Profile Already Exists.";
                    return RedirectToAction(nameof(SearchProjectAssignment));
                }

                ProjectUser puser = new ProjectUser();
                puser.Id = Guid.NewGuid().ToString().ToLower();
                puser.User = _context.Users.Single(i => i.Id == vm.Consultant);
                puser.Project = _context.Project.Single(i => i.Id == vm.Project);
                puser.Role = _context.SystemCodeDetails.Single(i => i.Id == vm.Role);
                new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, puser);

                
                _context.Add(puser);
                await _context.SaveChangesAsync();
                TempData["Success"] = "User/Project Profile Saved Successfully!";
                return RedirectToAction(nameof(SearchProjectAssignment), new { id = puser.Id });

            }

            ViewBag.DepartmentTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Department Categories"), "Id", "Code");
            ViewBag.ConsultantTypeId = new SelectList(_selectList, "Value", "Text");
            ViewBag.ProjectTypeId = new SelectList(_context.Project, "Id", "Name");
            ViewBag.RoleTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Project Role Categories"), "Id", "Code");
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/ProjectAssignment/Edit")]
        [Permission("Settings:Create")]
        public IActionResult EditProjectAssignment(string id, string returnUrl = null)
        {
            EditProjectAssignmentViewModel vm = new EditProjectAssignmentViewModel();
            vm.ProjectAssignmentId = id;
            var projectassignment = _context.ProjectUser
                .Include(p => p.Project)
                .Include(p => p.Project.ProjectDepartment.SystemCodeDetail)
                .Include(u => u.User)
                .Include(s => s.Role)
               .Single(i=>i.Id==id);
            new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(projectassignment, vm);
            vm.Department = projectassignment.Project.ProjectDepartment.SystemCodeDetail.Id;
            vm.Project = projectassignment.Project.Id;
            vm.Consultant = projectassignment.User.Id;
            vm.Role = projectassignment.Role.Id;

            var _selectList = _context.Users.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });

            ViewBag.DepartmentTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Department Categories"), "Id", "Code");
            ViewBag.ConsultantTypeId = new SelectList(_selectList, "Value", "Text");
            ViewBag.ProjectTypeId = new SelectList(_context.Project, "Id", "Name");
            ViewBag.RoleTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Project Role Categories"), "Id", "Code");

            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/ProjectAssignment/Edit")]
        [Permission("Settings:Create")]
        public IActionResult EditProjectAssignment(EditProjectAssignmentViewModel vm, string returnUrl = null)
        {

            if (ModelState.IsValid)
            {
                var projectassignment = _context.ProjectUser
                                .Include(p => p.Project)
                                .Include(p => p.Project.ProjectDepartment.SystemCodeDetail)
                                .Include(u => u.User)
                                .Include(s => s.Role)
                               .Single(i => i.Id == vm.ProjectAssignmentId);

                new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, projectassignment);
                projectassignment.Project.ProjectDepartment.SystemCodeDetail = _context.SystemCodeDetails.Single(i => i.Id == vm.Department);
                projectassignment.Project.ProjectDepartment.Project = _context.Project.Single(p => p.Id == vm.Project);
                projectassignment.Project = _context.Project.Single(p => p.Id == vm.Project);
                projectassignment.User = _context.Users.Single(u => u.Id == vm.Consultant);
                projectassignment.Role = _context.SystemCodeDetails.Single(i => i.Id == vm.Role);
                _uow.GetRepository<ProjectUser>().Update(projectassignment);
                _context.SaveChanges();

                TempData["Success"] = "User/Project Profile Updated Successfully!";
                return RedirectToAction(nameof(SearchProjectAssignment), new { id = projectassignment.Id });
            }
            

            var _selectList = _context.Users.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.DisplayName
            });

            ViewBag.DepartmentTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Department Categories"), "Id", "Code");
            ViewBag.ConsultantTypeId = new SelectList(_selectList, "Value", "Text");
            ViewBag.ProjectTypeId = new SelectList(_context.Project, "Id", "Name");
            ViewBag.RoleTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Project Role Categories"), "Id", "Code");

            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/Period/Search")]
        [Permission("Settings:Create")]
        public IActionResult SearchPeriod(string id, string returnUrl = null)
        {
            SearchPeriodViewModel vm = new SearchPeriodViewModel();
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;
            var periods = _context.Period.AsQueryable();

            vm.Period = periods.ToPagedList(page, pageSize);
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Period/Search")]
        [Permission("Settings:Create")]
        public IActionResult SearchPeriod(SearchPeriodViewModel vm, string returnUrl = null)
        {
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;
            var periods = _context.Period.AsQueryable();

            if (!string.IsNullOrEmpty(vm.Name))
            {
                periods = periods.Where(i => i.Name.Contains(vm.Name, StringComparison.OrdinalIgnoreCase));
            }
            vm.Period = periods.ToPagedList(page, pageSize);
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/Period/New")]
        [Permission("Settings:Create")]
        public IActionResult NewPeriod(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Period/New")]
        [Permission("Settings:Create")]
        public async Task<IActionResult> NewPeriod(NewPeriodViewModel vm, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                Period period = new Period();
                new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, period);
                period.Active = true;
                _context.Add(period);
                await _context.SaveChangesAsync();

                TempData["Success"] = "Period Saved Successfully!";
                return RedirectToAction(nameof(SearchPeriod), new { id = period.Id });
            }
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/Period/Edit")]
        [Permission("Settings:Create")]
        public IActionResult EditPeriod(int id, string returnUrl = null)
        {
            EditPeriodViewModel vm = new EditPeriodViewModel();
            vm.PeriodId = id;
            var period = _context.Period.Single(i => i.Id == id);
            new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(period, vm);

            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("Administration/Period/Edit")]
        [Permission("Settings:Create")]
        public IActionResult EditPeriod(EditPeriodViewModel vm, string returnUrl = null)
        {
            var period = _context.Period.Single(i => i.Id == vm.PeriodId);
            new AutoMapper.MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, period);
            _uow.GetRepository<Period>().Update(period);
            _context.SaveChanges();

            TempData["Success"] = "Period Updated Successfully!";
            return RedirectToAction(nameof(SearchPeriod), new { id = period.Id });
        }

        [Authorize]
        public IActionResult ActivatePeriod(int id)
        {
            var period = _context.Period.Find(id);
            period.Active = true;
            var auditTrail = new AuditTrail
            {
                ChangeType = "PeriodActivation",
                TableName = "Period",
                Description = "Period Activated",
                UserId = User.GetUserId()
            };
            _dbService.AuditTrail(auditTrail);
            _context.SaveChanges();

            TempData["Success"] = "Period Activated Successfully!";
            return RedirectToAction(nameof(SearchPeriod), new { id = period.Id });
        }

        [Authorize]
        public IActionResult DeActivatePeriod(int id)
        {
            var period = _context.Period.Find(id);
            period.Active = false;
            var auditTrail = new AuditTrail
            {
                ChangeType = "PeriodActivation",
                TableName = "Period",
                Description = "Period Activated",
                UserId = User.GetUserId()
            };
            _dbService.AuditTrail(auditTrail);
            _context.SaveChanges();

            TempData["Success"] = "Period De-Activated Successfully!";
            return RedirectToAction(nameof(SearchPeriod), new { id = period.Id });
        }

        [Authorize]
        [HttpGet]
        [Route("Administration/GetProjects")]
        public JsonResult GetProjects(int unit)
        {
            try
            {
                var _projects = _context.Project
                    .Include(u => u.ProjectDepartment)
                    .Where(u => u.ProjectDepartment.SystemCodeDetail.Id == unit)
                    .Select(i => new { i.Id, i.Name });

                return Json(_projects);
            }
            catch(Exception ex)
            {
                return Json(ex);
            }
        }
    }
}