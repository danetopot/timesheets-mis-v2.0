﻿using DP_TIMESHEET_MIS.Data;
using DP_TIMESHEET_MIS.Interfaces;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using DP_TIMESHEET_MIS.ViewModels.Dashboard;
using DP_TIMESHEET_MIS.Extensions;

namespace DP_TIMESHEET_MIS.Areas.Dashboard.Controllers
{
    [Area("Dashboard")]
    public class DashboardController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWork _uow;
        private readonly IDBService _dbService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IEmailService _emailService;

        public DashboardController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            IUnitOfWork uow,
            IDBService dbService,
            ILogger<TimesheetController> logger,
            IEmailService emailService,
            IHostingEnvironment hostingEnvironment)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
            _uow = uow;
            _dbService = dbService;
            _emailService = emailService;
            _hostingEnvironment = hostingEnvironment;
        }

        [Authorize]
        [Route("Dashboard")]
        public IActionResult Dashboard()
        {
            //var user = _context.Users
            //    .Include(p => p.Position.SystemCode)
            //    .Include(t => t.Type)
            //    .Include(s => s.SkillSet.SystemCode)
            //    .Single(i => i.Id == User.GetUserId());


            var timesheet = _context.TimesheetHeader.Where(i => i.IsActive == true).AsQueryable();
            var TotalPending = timesheet.Where(s => s.TimesheetStatus == "Pending Approval");
            var TotalApproved = timesheet.Where(s => s.ApprovedBy != null);
            var TotalRejected = timesheet.Where(s => s.RejectedBy != null);
            var TotalIncomplete = timesheet.Where(s => s.TimesheetStatus == "Incomplete");

            DashboardViewModel vm = new DashboardViewModel();
            vm.TotalPending = TotalPending.Count();
            vm.TotalApproved = TotalApproved.Count();
            vm.TotalRejected = TotalRejected.Count();
            vm.TotalIncomplete = TotalIncomplete.Count();
            ViewBag.PeriodTypeId = new SelectList(_context.Period.Where(i => i.Active == true), "Id", "Description");
            return View(vm);
        }

        [HttpGet]
        [Route("Dashboard/GetDashboard")]
        public JsonResult GetDashboard(int period)
        {
            try
            {
                var dashboard = _context.TimesheetHeader
                .Join(
                _context.TimeSheetDetail,
                f => f.Id,
                d => d.TimesheetHeader.Id,
                (f, d) => new
                {
                    Id = f.Id,
                    ProjectId = f.Project.Id,
                    ProjectName = f.Project.Name,
                    Period = f.Period.Id,
                    Status = f.TimesheetStatus,
                    f.ApprovedBy,
                    f.RejectedBy,
                    d.HoursWorked
                })
                //.Where(p => p.Period == 2)
                .GroupBy(f => f.ProjectId)
                .Select(x => new
                {
                    ProjectId = x.Key,
                    ProjectName = x.Select(g => g.ProjectName).First(),
                    Period = x.Select(g => g.Period).First(),
                    Status = x.Select(g => g.Status).First(),
                    ApprovedBy = x.Select(g => g.ApprovedBy).First(),
                    RejectedBy = x.Select(g => g.RejectedBy).First(),
                    HoursWorked = x.Select(g => g.HoursWorked).Sum()
                })
                .AsEnumerable();

                List<string> labels = new List<string>();
                String label = "Time spent in the project in hours";
                List<string> backgroundcolors = new List<string>();
                List<decimal> hoursworked = new List<decimal>();


                if (period > 0)
                {
                    //dashboard = dashboard.Where(p => p.Period == period);
                    dashboard = _context.TimesheetHeader
                    .Join(
                    _context.TimeSheetDetail,
                    f => f.Id,
                    d => d.TimesheetHeader.Id,
                    (f, d) => new
                    {
                        Id = f.Id,
                        ProjectId = f.Project.Id,
                        ProjectName = f.Project.Name,
                        Period = f.Period.Id,
                        Status = f.TimesheetStatus,
                        f.ApprovedBy,
                        f.RejectedBy,
                        d.HoursWorked
                    })
                    .Where(p => p.Period == period)
                    .GroupBy(f => f.ProjectId)
                    .Select(x => new
                    {
                        ProjectId = x.Key,
                        ProjectName = x.Select(g => g.ProjectName).First(),
                        Period = x.Select(g => g.Period).First(),
                        Status = x.Select(g => g.Status).First(),
                        ApprovedBy = x.Select(g => g.ApprovedBy).First(),
                        RejectedBy = x.Select(g => g.RejectedBy).First(),
                        HoursWorked = x.Select(g => g.HoursWorked).Sum()
                    })
                    .AsEnumerable();
                }

                if (dashboard != null)
                {
                    foreach (var item in dashboard)
                    {
                        var random = new Random();
                        var color = String.Format("#{0:X6}", random.Next(0x1000000));
                        labels.Add(item.ProjectName);
                        backgroundcolors.Add(color);
                        hoursworked.Add(item.HoursWorked);
                    }

                    decimal Yaxis = Math.Floor(hoursworked.Max() / 100) * 100;
                    if (Yaxis == 0)
                        Yaxis = 100;

                    DashboardViewModel vm = new DashboardViewModel
                    {
                        Labels = labels,
                        Label = label,
                        BackgroundColors = backgroundcolors,
                        HoursWorked = hoursworked,
                        YaxisMax = Yaxis
                    };

                    return Json(vm);
                }
                else
                {
                    return Json("Error");
                }
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }
    }
}