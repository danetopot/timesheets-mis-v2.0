﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System.IO;
using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using OfficeOpenXml;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.ViewModels.Report;
using DP_TIMESHEET_MIS.Services;
using DP_TIMESHEET_MIS.Data;
using DP_TIMESHEET_MIS.Extensions;
using static DP_TIMESHEET_MIS.Extensions.ExtensionMethods;


namespace DP_TIMESHEET_MIS.Areas.Dashboard.Controllers
{
    [Area("Report")]
    public class ReportController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;
        private readonly IDBService _dbService;
        private readonly IHostingEnvironment _hostingEnvironment;


        public ReportController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            IDBService dbService,
            IHostingEnvironment hostingEnvironment,
            ILogger<ReportController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
            _dbService = dbService;
            _hostingEnvironment = hostingEnvironment;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [Route("Report/Generate")]
        [Permission("Reports:Run")]
        public IActionResult Report(string returnUrl = null)
        {
            ViewBag.ReportTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Report Categories"), "Id", "Code");
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [Route("Report/Generate")]
        [Permission("Reports:Run")]
        public IActionResult Report(ReportViewModel vm, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var project = _context.Project
                    .Include(d => d.ProjectDepartment.SystemCodeDetail)
                    .Include(o => o.ProjectOfficer)
                    .Include(t => t.TeamLeader)
                    .Include(c => c.Country);

                var todaydate = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss").Replace('-', '_');
                var filename = @"ProjectReport_" + todaydate + ".pdf";
                var destPath = Path.Combine(_hostingEnvironment.WebRootPath + @"\report_templates\" + filename);
                var fileBytes = GenerateProjectReport(1, destPath, _hostingEnvironment, project);

                return File(fileBytes, "application/pdf", filename);
            }
            ViewBag.ReportTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Report Categories"), "Id", "Code");
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [Route("Report/Print")]
        public IActionResult Print(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
    }
}