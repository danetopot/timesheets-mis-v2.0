﻿using DP_TIMESHEET_MIS.Data;
using DP_TIMESHEET_MIS.Extensions;
using DP_TIMESHEET_MIS.Interfaces;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Services;
using DP_TIMESHEET_MIS.ViewModels.Timesheet;
using static DP_TIMESHEET_MIS.Extensions.ExtensionMethods;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using PagedList.Core;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
using Microsoft.AspNetCore.Authorization;

namespace DP_TIMESHEET_MIS.Areas.Dashboard.Controllers
{
    [Area("Timesheet")]
    public class TimesheetController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWork _uow;
        private readonly IDBService _dbService;
        private readonly ILogService _logService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IEmailService _emailService;

        public TimesheetController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            IUnitOfWork uow,
            IDBService dbService,
            ILogger<TimesheetController> logger,
            ILogService logService,
            IEmailService emailService,
            IHostingEnvironment hostingEnvironment)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
            _uow = uow;
            _dbService = dbService;
            _emailService = emailService;
            _logService = logService;
            _hostingEnvironment = hostingEnvironment;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        public IActionResult Debug(string debug)
        {
            return Content(debug);
        }

        [Authorize]
        [HttpGet]
        [Route("Registration/UploadSignature")]
        public IActionResult Signature(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Registration/UploadSignature")]
        public async Task<IActionResult> Signature(SignatureViewModel vm, IFormFile Signature, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                int imgWidth = 0;
                int imgHeight = 0;
                using (var image = Image.FromStream(Signature.OpenReadStream()))
                {
                    imgWidth = image.Width;
                    imgHeight = image.Height;
                }
                if(imgWidth > 32 || imgHeight > 32)
                {
                    TempData["Error"] = "Signature Exceeded Required Image Size!";
                    return RedirectToAction(nameof(Signature));
                }

                string webRootPath = _hostingEnvironment.WebRootPath;
                var user = _context.Users.Single(u => u.Id == User.GetUserId());
                user.Signature = await GetFileAsync(
                    webRootPath,
                    Signature,
                    user.Email,
                    "Signature"
                );

                await _context.SaveChangesAsync();
                TempData["Success"] = "Signature Saved Successfully!";
                return RedirectToAction(nameof(Search));
                // return Ok();
            }
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
            //return Ok();
        }

        [Authorize]
        [Route("Timesheet/Search/{id?}")]
        [Permission("Timesheet:Search")]
        public async Task<IActionResult> Search(SearchTimesheetViewModel vm, string id, string returnUrl = null)
        {
           
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;
            var userId = User.GetUserId();
            var currentUser = _context.Users.SingleOrDefault(i => i.Id == userId);
            var signature = currentUser.Signature;
            if (string.IsNullOrEmpty(signature))
            {
                return RedirectToAction(nameof(Signature));
            }

            var isApprover = false;
            var currentUserRoles = await _userManager.GetRolesAsync(currentUser);
            if (currentUserRoles.Contains("Supervisor"))
            {
                isApprover = true;
            }
            IList<string> timesheetuserids = GetTimesheetUserIds(_context, currentUser, currentUserRoles);

            var timesheet = _context.TimesheetHeader
            .Join(
                _context.TimeSheetDetail,
                f => f.Id,
                d => d.TimesheetHeader.Id,
                (f, d) => new
                {
                    Id = f.Id,
                    ConsultantId = f.Consultant.Id,
                    f.Consultant.DisplayName,
                    f.Consultant.FirstName,
                    f.Consultant.Surname,
                    Project = f.Project.Name,
                    ProjectId = f.Project.Id,
                    Period = f.Period.Id,
                    PeriodName = f.Period.Name,
                    d.HoursWorked,
                    d.TaskDescription,
                    d.Site,
                    f.TimesheetStatus,
                    f.ApprovedBy,
                    HeaderId = f.Id,
                    CreatedBy = f.CreatedBy.CreatedById,
                    isApprover =isApprover,
                    page = page,
                    pageSize = pageSize
                })
            .Where(c => timesheetuserids.Contains(c.ConsultantId))
            .GroupBy(f => f.Id)
            .Select(x => new SearchTableViewModel
            {
                Id = x.Key,
                Name = x.Select(g => g.DisplayName).First(),
                Project = x.Select(g => g.Project).First(),
                Period = x.Select(g => g.Period).First(),
                PeriodName = x.Select(g => g.PeriodName).First(),
                HeaderId = x.Select(g => g.HeaderId).First(),
                TimesheetStatus = x.Select(g => g.TimesheetStatus).First(),
                ApprovedBy = x.Select(g => g.ApprovedBy).First(),
                HoursWorked = x.Select(g => g.HoursWorked).Sum(),
                CreatedBy = x.Select(g => g.CreatedBy).First(),
                isApprover = x.Select(g => g.isApprover).First(),
            }).OrderByDescending(c => c.Name).AsQueryable();
           

            if (isApprover)
            {
                timesheet = timesheet.Where(f => f.TimesheetStatus == "Pending Approval" || f.CreatedBy== User.GetUserId());
            }

            ViewData["ReturnUrl"] = returnUrl;
            vm.Timesheet = timesheet.ToPagedList(page, pageSize);
            ViewBag.PeriodTypeId = new SelectList(_context.Period.Where(i => i.Active == true), "Id", "Description");
            ViewBag.DepartmentTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Department Categories"), "Id", "Code");
            ViewBag.ProjectTypeId = new SelectList(_context.Project, "Id", "Name");
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Timesheet/Search")]
        [Permission("Timesheet:Search")]
        public async Task<IActionResult> Search(SearchTimesheetViewModel vm, string returnUrl = null)
        {
            var userId = User.GetUserId();
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;
            var currentUser = _context.Users.SingleOrDefault(i => i.Id == userId);
            var isApprover = false;

            var currentUserRoles = await _userManager.GetRolesAsync(currentUser);
            if (currentUserRoles.Contains("Supervisor"))
            {
                isApprover = true;
            }
            IList<string> timesheetuserids = GetTimesheetUserIds(_context, currentUser, currentUserRoles);

            var timesheet = _context.TimesheetHeader
            .Join(
                _context.TimeSheetDetail,
                f => f.Id,
                d => d.TimesheetHeader.Id,
                (f, d) => new
                {
                    Id = f.Id,
                    ConsultantId = f.Consultant.Id,
                    f.Consultant.DisplayName,
                    f.Consultant.FirstName,
                    f.Consultant.Surname,
                    Project = f.Project.Name,
                    ProjectId = f.Project.Id,
                    Period = f.Period.Id,
                    PeriodName = f.Period.Name,
                    d.HoursWorked,
                    d.TaskDescription,
                    d.Site,
                    f.TimesheetStatus,
                    f.ApprovedBy,
                    HeaderId = f.Id,
                    CreatedBy = f.CreatedBy,
                    isApprover = isApprover
                })
            .Where(c => timesheetuserids.Contains(c.ConsultantId))
            .GroupBy(f => f.Id)
            .Select(x => new SearchTableViewModel
            {
                Id = x.Key,
                Name = x.Select(g => g.DisplayName).First(),
                Project = x.Select(g => g.Project).First(),
                ProjectId = x.Select(g => g.ProjectId).First(),
                Period = x.Select(g => g.Period).First(),
                PeriodName = x.Select(g => g.PeriodName).First(),
                HeaderId = x.Select(g => g.HeaderId).First(),
                TimesheetStatus = x.Select(g => g.TimesheetStatus).First(),
                ApprovedBy = x.Select(g => g.ApprovedBy).First(),
                HoursWorked = x.Select(g => g.HoursWorked).Sum()
            }).OrderByDescending(c => c.Name).AsQueryable();

            if (!string.IsNullOrEmpty(vm.SearchName))
            {
                timesheet = timesheet.Where(f => f.Name.ToLower().Contains(vm.SearchName.ToLower()));
            }

            if (!string.IsNullOrEmpty(vm.Project))
            {
                timesheet = timesheet.Where(f => f.ProjectId == vm.Project);
            }

            if (vm.Period != 0)
            {
                timesheet = timesheet.Where(f => f.Period == vm.Period);
            }

            if (isApprover)
            {
                timesheet = timesheet.Where(f => f.TimesheetStatus == "Pending Approval");
            }

            ViewData["ReturnUrl"] = returnUrl;
            vm.Timesheet = timesheet.ToPagedList(page, pageSize);
            ViewBag.PeriodTypeId = new SelectList(_context.Period.Where(i => i.Active == true), "Id", "Description");
            ViewBag.DepartmentTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Department Categories"), "Id", "Code");
            ViewBag.ProjectTypeId = new SelectList(_context.Project, "Id", "Name");
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Timesheet/New")]
        [Permission("Timesheet:Create")]
        public IActionResult New(string returnUrl = null)
        {
            var _projectList = _context.ProjectUser.Where(u => u.User.Id == User.GetUserId())
                .Include(p => p.Project)
                .Include(p => p.Role)
                .Select(s => new SelectListItem
                {
                    Value = s.Project.Id,
                    Text = s.Project.Name
                });

            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.PeriodTypeId = new SelectList(_context.Period.Where(i => i.Active == true), "Id", "Description");
            ViewBag.ProjectTypeId = new SelectList(_projectList, "Value", "Text");
            ViewBag.SiteTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Site Categories"), "Id", "Code");
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Timesheet/New")]
        [Permission("Timesheet:Create")]
        public async Task<IActionResult> New(NewTimesheetViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var _user = _context.Users.Single(i => i.Id == User.GetUserId());
                var _statistics = GetDbStore(_context, User.GetUserId(), (int)vm.Period, vm.Project);

                if (Convert.ToInt32(_statistics["HeaderCount"]) == 0)
                {
                    TimesheetHeader header = new TimesheetHeader();
                    header.Id = Guid.NewGuid().ToString().ToLower();
                    header.Period = _context.Period.Single(i => i.Id == vm.Period);
                    header.Consultant = _user;
                    header.TimesheetStatus = _statistics["PeriodStatus"];
                    header.IsActive = true;
                    header.Project = _context.Project.Single(i => i.Id == vm.Project);
                    header.DateCreated = DateTime.Now;
                    header.CreatedBy = _user;
                    _context.Add(header);
                    await _context.SaveChangesAsync();

                    TimeSheetDetail detail = new TimeSheetDetail();
                    new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, detail);
                    detail.TimesheetHeader = _context.TimesheetHeader.Single(i => i.Id == header.Id);
                    detail.Project = _context.Project.Single(i => i.Id == vm.Project);
                    detail.Site = _context.SystemCodeDetails.Single(i => i.Id == vm.Site);
                    detail.CreatedBy = _user;
                    detail.DateCreated = DateTime.Now;
                    _context.Add(detail);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Edit), new { id = header.Id });

                }
                else
                {
                    TimeSheetDetail detail = new TimeSheetDetail();
                    new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, detail);
                    string _headerId = _statistics["HeaderID"];
                    detail.TimesheetHeader = _context.TimesheetHeader.Single(i => i.Id == _headerId);
                    detail.TimesheetHeader.Period = _context.Period.Single(i => i.Id == vm.Period);
                    detail.Project = _context.Project.Single(i => i.Id == vm.Project);
                    detail.Site = _context.SystemCodeDetails.Single(i => i.Id == vm.Site);
                    detail.CreatedBy = _user;
                    detail.DateCreated = DateTime.Now;
                    _context.Add(detail);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Edit), new { id = _headerId });
                }
            }

            var _projectList = _context.ProjectUser.Where(u => u.User.Id == User.GetUserId())
                .Include(p => p.Project)
                .Include(p => p.Role)
                .Select(s => new SelectListItem
                {
                    Value = s.Project.Id,
                    Text = s.Project.Name
                });

            ViewBag.PeriodTypeId = new SelectList(_context.Period.Where(i => i.Active == true), "Id", "Description");
            ViewBag.ProjectTypeId = new SelectList(_projectList, "Value", "Text");
            ViewBag.SiteTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Site Categories"), "Id", "Code");
            return View(vm);
        }

        [Authorize]
        [Route("Timesheet/View")]
        [Permission("Timesheet:View")]
        public IActionResult View(string id, string returnUrl = null)
        {
            var vm = new ViewTimesheetViewModel();
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;
            var userId = User.GetUserId();
            var _consultant = _context.Users.Single(u => u.Id == userId);
            //return Debug(_consultant.Position.Code);


            var timeSheet = _context.TimeSheetDetail.
                OrderByDescending(c => c.DateCreated)
                .Include(c => c.Project)
                .Include(c => c.Site)
                .Include(c => c.TimesheetHeader.Consultant.Type)
               .Where(c => c.TimesheetHeader.Id == id)
               .AsQueryable();

            foreach (var t in timeSheet)
            {
                vm.Name = t.TimesheetHeader.Consultant.DisplayName;
                vm.Type = t.TimesheetHeader.Consultant.Type.Code;
            }

            vm.HeaderId = id;
            vm.Timesheets = timeSheet.ToPagedList(page, pageSize);
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [Route("Timesheet/Edit/{id}")]
        [Permission("Timesheet:Modify")]
        public IActionResult Edit(EditTimesheetViewModel vm, string returnUrl = null)
        {
            var id = vm.Id;
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;
            var userId = User.GetUserId();
            var _consultant = _context.Users.Single(u => u.Id == userId);

            var timeSheet = _context.TimeSheetDetail.
                OrderByDescending(c => c.DateCreated)
                .Include(c => c.Project)
                .Include(c => c.Site)
                .Include(c => c.TimesheetHeader.Period)
                .Include(c => c.TimesheetHeader.Consultant.Type)
                .Include(c => c.TimesheetHeader.Project.ProjectDepartment)
               .Where(c => c.TimesheetHeader.Id == id).OrderBy(d => d.DateFilled)
               .AsQueryable();

            foreach (var t in timeSheet)
            {
                vm.Name = t.TimesheetHeader.Consultant.DisplayName;
                vm.Type = t.TimesheetHeader.Consultant.Type.Code;
                vm.Project = t.TimesheetHeader.Project.Name;
                vm.Period = t.TimesheetHeader.Period.Name;
            }

            vm.HeaderId = id;
            if (page == 0) { page = 1; }
            vm.Timesheets = timeSheet.ToPagedList(page, pageSize);
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [HttpGet]
        [Route("Timesheet/EditDetail")]
        [Permission("Timesheet:Modify")]
        public IActionResult EditDetail(string id, string returnUrl = null)
        {
            var vm = new EditTimesheetDetailViewModel();
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;

            var timesheet = _context.TimeSheetDetail
                .Include(p => p.Project)
                .Include(s => s.Site)
                .Include(t => t.TimesheetHeader)
                .Include(t => t.TimesheetHeader.Period)
                .Include(t => t.TimesheetHeader.CreatedBy)
                .SingleOrDefault(i => i.Id == id);
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(timesheet, vm);

            vm.Consultant = timesheet.CreatedBy.Id;
            vm.Period = timesheet.TimesheetHeader.Period.Id;
            vm.Project = timesheet.Project.Id;
            vm.Site = timesheet.Site.Id;
            vm.DetailId = id;
            vm.HeaderId = timesheet.TimesheetHeader.Id;

            var _projectList = _context.ProjectUser.Where(u => u.User.Id == User.GetUserId())
                .Include(p => p.Project)
                .Include(p => p.Role)
                .Select(s => new SelectListItem
                {
                    Value = s.Project.Id,
                    Text = s.Project.Name
                });

            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.PeriodTypeId = new SelectList(_context.Period.Where(i => i.Active == true), "Id", "Description");
            ViewBag.ProjectTypeId = new SelectList(_projectList, "Value", "Text");
            ViewBag.RoleTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Project Role Categories"), "Id", "Code");
            ViewBag.SiteTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Site Categories"), "Id", "Code");
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Timesheet/EditDetail")]
        [Permission("Timesheet:Modify")]
        public async Task<IActionResult> EditDetail(EditTimesheetDetailViewModel vm, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var detail = _context.TimeSheetDetail
                   .Include(p => p.Project)
                   .Include(s => s.Site)
                   .Include(t => t.TimesheetHeader)
                   .Include(t => t.TimesheetHeader.Period)
                   .Include(t => t.TimesheetHeader.CreatedBy)
                   .SingleOrDefault(i => i.Id == vm.DetailId);

                var header = _context.TimesheetHeader
                    .Where(
                    h => h.Project.Id == vm.Project &&
                    h.Period.Id == vm.Period &&
                    h.Consultant.Id == vm.Consultant
                    ).SingleOrDefault();

                if (header !=null)
                {
                    header.Project = _context.Project.Single(i => i.Id == vm.Project);
                    _uow.GetRepository<TimesheetHeader>().Update(header);
                    _context.SaveChanges();

                    new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, detail);
                    _uow.GetRepository<TimeSheetDetail>().Update(detail);
                    detail.TimesheetHeader = header;
                    detail.Project = _context.Project.Single(i => i.Id == vm.Project);
                    detail.Site = _context.SystemCodeDetails.Single(i => i.Id == vm.Site);
                    detail.TimesheetHeader.Period = _context.Period.Single(i => i.Id == vm.Period);
                    _context.SaveChanges();
                }
                else
                {
                    var _user = _context.Users.Single(i => i.Id == User.GetUserId());
                    var _statistics = GetDbStore(_context, User.GetUserId(), (int)vm.Period, vm.Project);

                    TimesheetHeader _header = new TimesheetHeader();
                    _header.Id = Guid.NewGuid().ToString().ToLower();
                    _header.Period = _context.Period.Single(i => i.Id == vm.Period);
                    _header.Consultant = _user;
                    _header.TimesheetStatus = _statistics["PeriodStatus"];
                    _header.IsActive = true;
                    _header.Project = _context.Project.Single(i => i.Id == vm.Project);
                    _header.DateCreated = DateTime.Now;
                    _header.CreatedBy = _user;
                    _context.Add(_header);
                    await _context.SaveChangesAsync();

                    new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(vm, detail);
                    _uow.GetRepository<TimeSheetDetail>().Update(detail);
                    detail.TimesheetHeader = _header;
                    detail.Project = _context.Project.Single(i => i.Id == vm.Project);
                    detail.Site = _context.SystemCodeDetails.Single(i => i.Id == vm.Site);
                    detail.TimesheetHeader.Period = _context.Period.Single(i => i.Id == vm.Period);
                    _context.SaveChanges();
                }

                TempData["Success"] = "Timesheet Updated Successfully!";
                ViewData["ReturnUrl"] = returnUrl;
                return RedirectToAction(nameof(Edit), new { id = vm.HeaderId });
            }

            var _projectList = _context.ProjectUser.Where(u => u.User.Id == User.GetUserId())
                .Include(p => p.Project)
                .Include(p => p.Role)
                .Select(s => new SelectListItem
                {
                    Value = s.Project.Id,
                    Text = s.Project.Name
                });

            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.PeriodTypeId = new SelectList(_context.Period.Where(i => i.Active == true), "Id", "Description");
            ViewBag.ProjectTypeId = new SelectList(_projectList, "Value", "Text");
            ViewBag.SiteTypeId = new SelectList(_context.SystemCodeDetails.Where(i => i.SystemCode.Code == "Site Categories"), "Id", "Code");
            return View(vm);
        }

        [Authorize]
        [Route("Timesheet/Delete")]
        [Permission("Timesheet:Delete")]
        public IActionResult Delete(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [Authorize]
        [Route("Timesheet/SendToApproval")]
        [Permission("Timesheet:Send For Approval")]
        public async Task<IActionResult> SendToApproval(string id, string returnUrl = null)
        {
            var _user = _context.Users.Single(u => u.Id == User.GetUserId());
            var timesheet = _context.TimesheetHeader
                .Include(p => p.Period)
                .Include(x => x.Project)
                .SingleOrDefault(i => i.Id == id);
            _uow.GetRepository<TimesheetHeader>().Update(timesheet);
            timesheet.TimesheetStatus = "Pending Approval";
            var _period = timesheet.Period.Description;
            _context.SaveChanges();

            //Send Email
            var _linemanagerid = _user.LineManagerId;
            var _linemanager = _context.Users.Single(l => l.Id == _linemanagerid);
            var _linemanageremail = _linemanager.Email;
            var subject = "Timesheet Pending Approval";
            var message = string.Format("Dear {0}, {1} has submitted {2} timesheet for {3} for your approval.Login to http://timesheet.developmentpathways.co.ke/ to approve.Thank you.",
               (_linemanager.DisplayName).ToTitleCase(),
               _user.DisplayName,
               timesheet.Project.Name,
               _period.ToTitleCase()
               );
            //await _emailService.SendEmailAsync("dopot@developmentpathways.co.uk", subject, message);
            await _emailService.SendEmailAsync(_linemanageremail, subject, message);

            String msg = "Timesheet sent for approval successfully!";
            _logService.FileLog(msg);


            TempData["Success"] = "Timesheet sent for approval successfully!";
            ViewData["ReturnUrl"] = returnUrl;
            return RedirectToAction(nameof(Search), new { id = id });
        }

        [Authorize]
        [Route("Timesheet/Approve")]
        [Permission("Timesheet:Approve")]
        public IActionResult Approve(string id, string returnUrl = null)
        {
            var vm = new ApproveTimesheetViewModel();
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 10;
            var userId = User.GetUserId();
            var _consultant = _context.Users.Single(u => u.Id == userId);
            //return Debug(_consultant.Position.Code);


            var timeSheet = _context.TimeSheetDetail.
                OrderByDescending(c => c.DateCreated)
                .Include(c => c.Project)
                .Include(c => c.Site)
                .Include(c => c.TimesheetHeader.Period)
                .Include(c => c.TimesheetHeader.Consultant.Type)
                .Include(c => c.TimesheetHeader.Project.ProjectDepartment)
               .Where(c => c.TimesheetHeader.Id == id)
               .AsQueryable();

            foreach (var t in timeSheet)
            {
                vm.Name = t.TimesheetHeader.Consultant.DisplayName;
                vm.Type = t.TimesheetHeader.Consultant.Type.Code;
                vm.Project = t.TimesheetHeader.Project.Name;
                vm.Period = t.TimesheetHeader.Period.Name;
            }

            vm.HeaderId = id;
            vm.Timesheets = timeSheet.ToPagedList(page, pageSize);
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Authorize]
        [Route("Timesheet/Approving")]
        [Permission("Timesheet:Approve")]
        public async Task<IActionResult> Approving(ApproveTimesheetViewModel vm, string id, string returnUrl = null)
        {
            try
            {
                var _user = _context.Users.Single(u => u.Id == User.GetUserId());
                var timesheet = _context.TimesheetHeader
                    .Include(x => x.Period)
                    .Include(x => x.CreatedBy)
                    .Include(x => x.Project)
                    .Include(x => x.Consultant)
                    .SingleOrDefault(i => i.Id == id);
                _uow.GetRepository<TimesheetHeader>().Update(timesheet);
                timesheet.TimesheetStatus = "Approved";
                timesheet.DateApproved = DateTime.Now;
                timesheet.ApprovedBy = _user;
                var _period = timesheet.Period.Description;
                _context.SaveChanges();

                TempData["Success"] = string.Format(
                    "Timesheet for {0} Approved Successfully!", (timesheet.CreatedBy.DisplayName).ToTitleCase()
                    );

                //Send Email
                var email = timesheet.Consultant.Email;
                var subject = "Timesheet Approved";
                var message = string.Format("Dear {0}, your {1} timesheet for {2} has been approved. Thank you.",
                   (timesheet.Consultant.DisplayName).ToTitleCase(),
                    timesheet.Project.Name,
                   _period.ToTitleCase()
                   );

                await _emailService.SendEmailAsync(email, subject, message);
                //await _emailService.SendEmailAsync("dopot@developmentpathways.co.uk", subject, message);

                String msg = "Timesheet approved and email sent";
                _logService.FileLog(msg);
            }
            catch(Exception ex)
            {
                ViewData["ReturnUrl"] = returnUrl;
                TempData["Error"] = "An error occured when approving the Timesheet!";
                _logService.FileLog("Error Timesheet/Approving :- " + ex.Message);
            }

            return RedirectToAction(nameof(Search));
        }

        [Authorize]
        [HttpPost]
        [Route("Timesheet/Reject")]
        [Permission("Timesheet:Approve")]
        public async Task<JsonResult> Reject([FromBody] Dictionary<string, string> data)
        {
            var _id = data["headerId"];
            var _rejectReason = data["rejectReason"];

            var _user = _context.Users.Single(u => u.Id == User.GetUserId());
            var timesheet = _context.TimesheetHeader
                .Include(p => p.Period)
                .Include(d => d.CreatedBy)
                .Include(x => x.Project)
                .SingleOrDefault(i => i.Id == _id.Trim('"'));
            _uow.GetRepository<TimesheetHeader>().Update(timesheet);
            timesheet.TimesheetStatus = "Rejected";
            timesheet.RejectReason = _rejectReason;
            timesheet.DateRejected = DateTime.Now;
            timesheet.RejectedBy = _context.Users.Single(u => u.Id == User.GetUserId());
            var _period = timesheet.Period.Description;
            var _rejectreason = timesheet.RejectReason;
            _context.SaveChanges();

            TempData["Success"] = string.Format(
                "Timesheet for {0} Rejected Successfully!", (timesheet.CreatedBy.DisplayName).ToTitleCase()
                );

            //Send Email
            var email = _user.Email;
            var subject = "Timesheet Rejected";
            var message = string.Format("Dear {0}, your {1} timesheet for {2} has been rejected.\nReasons : - {3}.\nThank you.",
               (_user.DisplayName).ToTitleCase(),
               timesheet.Project.Name,
               _period.ToTitleCase(),
               _rejectreason.ToTitleCase()
               );

            // string.Format("{0}, {1} is {2} years old", person.LastName, person.FirstName, person.Age);
            await _emailService.SendEmailAsync(email, subject, message);

            String msg = "Timesheet rejected and email sent";
            _logService.FileLog(msg);

            return new JsonResult(Url.Action("Search"));
        }

        [Authorize]
        [Route("Timesheet/Download")]
        public IActionResult Download(string id, string returnUrl = null)
        {
            try
            {
                var vm = new ViewTimesheetViewModel();
                var page = vm.Page ?? 1;
                var pageSize = vm.PageSize ?? 10;
                var userId = User.GetUserId();

                var _timesheetdetails = _context.TimeSheetDetail.
                    OrderByDescending(c => c.CreatedBy)
                    .Include(c => c.Project)
                    .Include(c => c.Site)
                    .Include(c => c.TimesheetHeader.Consultant.Type)
                   .Where(c => c.TimesheetHeader.Id == id).AsQueryable()
                   .OrderBy(d => d.DateFilled);

                var _timesheetheader = _context.TimesheetHeader.Where(t => t.Id == id)
                    .Include(t => t.Project)
                    .Include(p => p.Period)
                    .Include(c => c.Consultant)
                    .Include(i => i.Consultant.Position.SystemCode)
                    .AsQueryable();

                var _user = _context.Users.Include(p => p.Position).AsQueryable();
                var _header = _timesheetheader.Single();
                var _consultant = _user.Where(i => i.Id == _header.Consultant.Id).Single();
                var _username = _header.Consultant.FileName.ToTitleCase().Trim();
                var _projectname = _header.Project.Name.ToTitleCase().Trim();
                var _todaydate = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss").Replace('-', '_');
                var _filename = _username + @"_" + _projectname + @"_TimesheetReport_" + _todaydate + ".pdf";

                var destPath = Path.Combine(_hostingEnvironment.WebRootPath + @"\report_templates\" + _filename);
                var fileBytes = GenerateTimesheet(1, destPath, _hostingEnvironment, _user, _timesheetheader, _timesheetdetails);

                String msg = "Download successful. Report directory :- " + destPath;
                _logService.FileLog(msg);

                return File(fileBytes, "application/pdf", _filename);
            }
            catch(Exception ex)
            {
                String msg = "Error downloading timesheet :- " + ex.ToString();
                _logService.FileLog(msg);
                return null;
            }

        }

        #region Helpers
        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
        public class DisableFormValueModelBindingAttribute : Attribute, IResourceFilter
        {
            public void OnResourceExecuting(ResourceExecutingContext context)
            {
                var factories = context.ValueProviderFactories;
                factories.RemoveType<FormValueProviderFactory>();
                factories.RemoveType<JQueryFormValueProviderFactory>();
            }

            public void OnResourceExecuted(ResourceExecutedContext context)
            {
            }
        }
        #endregion Helpers
    }
}