﻿using DP_TIMESHEET_MIS.Data;
using DP_TIMESHEET_MIS.Extensions;
using DP_TIMESHEET_MIS.Interfaces;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Services;
using DP_TIMESHEET_MIS.ViewModels.Account;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using static DP_TIMESHEET_MIS.Extensions.ExtensionMethods;
using DP_TIMESHEET_MIS.ViewModels.Dashboard;

namespace DP_TIMESHEET_MIS.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWork _uow;
        private readonly IDBService _dbService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IEmailService _emailService;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            IUnitOfWork uow,
            IDBService dbService,
            ILogger<AccountController> logger,
            IEmailService emailService,
            IHostingEnvironment hostingEnvironment)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
            _uow = uow;
            _dbService = dbService;
            _emailService = emailService;
            _hostingEnvironment = hostingEnvironment;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> AccessDenied(string returnUrl = null)
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User Logged Out.Permission Issue.");

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            var model = new LoginViewModel();
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            ViewData["ReturnUrl"] = returnUrl;
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    var user = _context.Users.Single(u => u.UserName == model.Email);
                    string _userId = user.Id;
                    string _lastPswdChangedDate = user.LastPasswordChangedDate.ToString();
                    if (string.IsNullOrWhiteSpace(_lastPswdChangedDate))
                    {
                        return RedirectToAction("ResetPassword", new { id = _userId });
                    }

                    var userId = user.Id;
                    user.IsLoggedIn = true;
                    _logger.LogInformation("User logged in.");
                    var auditTrail = new AuditTrail
                    {
                        ChangeType = "Login",
                        TableName = "User",
                        Description = "Login Success",
                        UserId = userId
                    };

                    _dbService.AuditTrail(auditTrail);
                    _context.SaveChanges();

                    if (string.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("Dashboard", "Dashboard", new { area = "Dashboard" });
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    var user = _context.Users.SingleOrDefault(u => u.UserName == model.Email);
                    if (user != null)
                    {
                        var userId = user.Id;
                        var auditTrail = new AuditTrail
                        {
                            ChangeType = "Login",
                            TableName = "User",
                            Description = "Login Failure. Username:" + model.Email + "<br />" + result,
                            UserId = userId
                        };

                        _dbService.AuditTrail(auditTrail);
                    }

                    ModelState.AddModelError(string.Empty, "Invalid Login Attempt!");
                    return View(model);
                }
            }
            ViewData["ReturnUrl"] = returnUrl;
            return View(model);
        }

        public async Task<IActionResult> Logout()
        {
            var userId = User.GetUserId();
            var user = _context.Users.Find(userId);

            user.IsLoggedIn = false;
            user.LastActivityDate = DateTime.UtcNow;

            _logger.LogInformation("User logged in.");
            var auditTrail = new AuditTrail
            {
                ChangeType = "LogOff",
                TableName = "User",
                Description = "Log Off Success",
                UserId = userId
            };

            _dbService.AuditTrail(auditTrail);

            _context.SaveChanges();
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User Logged Out.");
            return RedirectToAction("Login");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string id, string returnUrl = null)
        {
            ResetPasswordViewModel vm = new ResetPasswordViewModel();
            var user = _context.Users.SingleOrDefault(u => u.Id == id);
            vm.Email = user.Email;
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel vm, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(vm.Email);
                var hashedPassword = _userManager.PasswordHasher.HashPassword(user, vm.Password);
                user.LastPasswordChangedDate = DateTime.Now;
                user.PasswordHash = hashedPassword;
                await _context.SaveChangesAsync();

                TempData["SuccessMessage"] = "Password Reset Successful!";
                return RedirectToAction("Login");
            }
            
            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword(string id, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel vm, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
               

                var result = _context.SystemCodeDetails.ToList();
                var user = await _userManager.FindByEmailAsync(vm.Email);
                if (user == null)
                {
                    TempData["ErrorMessage"] = "Email Address Does Not Exist!";
                    return RedirectToAction("Login");
                }

                var password = _dbService.GeneratePassword(6);
                //var _user = _context.Users.Find(user.Id);
                var hashedPassword = _userManager.PasswordHasher.HashPassword(user, password);
                user.PasswordHash = hashedPassword;
                user.LastPasswordChangedDate = null;
                _uow.GetRepository<ApplicationUser>().Update(user);
                _context.SaveChanges();

                string message = String.Format("Dear {0}, your password to login in to Timesheet Portal is {1} \nLogin to http://timesheet.developmentpathways.co.ke/ to reset.", user.FirstName, password);
                string subject = String.Format("{0}", "Recovery Password To Timesheet App");
                await _emailService.SendEmailAsync(vm.Email, subject, message);
                _logger.LogInformation("User created and password sent");

                TempData["SuccessMessage"] = "Recovery Password Sent Successful!";
                return RedirectToAction("Login");
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(vm);
        }

        [Route("Account/Profile")]
        public IActionResult Profile()
        {
            var userId = User.GetUserId();
            var user = _context.Users.Find(userId);
            ProfileViewModel vm = new ProfileViewModel();
            ViewBag.Text = "Sample Text";
            vm.Name = user.DisplayName.ToTitleCase();
            vm.Email = user.Email;
            return View();
        }

        #region Helpers
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        #endregion Helpers
    }
}