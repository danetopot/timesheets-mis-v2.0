﻿$(document).ready(function () {
    $('.lnk-modal').fadeIn();
    var n = '<div class="modal-dialog"><div class="modal-content"><div class="modal-header bg-info preloader"><h4 class="modal-title text-center">Loading....<\/h4><\/div><div class="modal-body"><p class="text-center"> <i class="fa  fa-cog fa-spin fa-3x fa-fw"><\/i><span class="sr-only">Loading...<\/span> <\/p><\/div><div class="modal-footer"><button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close<\/button><\/div><\/div><\/div>';

    $('.modal-link,.lnk-modal').off('click').on("click", function (t) {
        t.preventDefault();
        $("#popup").html(n);
        $("#popup").load($(this).attr("href"), function () {
             $(".checkbox").each(function () {
                if ($(this).prop("checked") === false) {
                    $(this).after('<input type="hidden" name="' + $(this).attr("name") + '" value="false">');
                }
             });
        });
        $("#popup").modal("show");
        $("#popup").modal({ keyboard: 0 });
    });

    $('.pagination a:not(.get .pagination a)').on('click', function () {
        var page = $.urlParam($(this).attr('href'), 'page');
        $('#pagination-page').val(page);
        $('.form-paginated').trigger('submit');
        return false;
    });

    $.urlParam = function (url, shows) {
        var results = new RegExp('[\\?&]' + shows + '=([^&#]*)').exec(url);
        if (!results) { return 0; } return results[1] || 0;
    }
});