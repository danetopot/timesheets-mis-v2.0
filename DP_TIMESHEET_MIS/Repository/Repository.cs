﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using DP_TIMESHEET_MIS.Data;
using DP_TIMESHEET_MIS.Interfaces;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Services;

namespace DP_TIMESHEET_MIS.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext Context = null;

        private readonly DbSet<TEntity> _dbSet;

        private readonly IHttpContextAccessor _http;

        private readonly IDBService _dbService;

        public Repository(ApplicationDbContext context, IDBService dbService)
        {
            Context = context;
            var dbContext = context as DbContext;
            _dbSet = dbContext.Set<TEntity>();
            _dbService = dbService;
        }


        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
            var AuditTrail = new AuditTrail
            {
                ChangeType = "Create",
                Description = "Created",
                TableName = entity.GetType().Name,
            };
            _dbService.AuditTrail(AuditTrail);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            _dbSet.AddRange(entities);
        }

        public void Update(TEntity entity)
        {
            var dbEntry = Context.Entry(entity);
            Context.Set<TEntity>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            var id = "";
            try
            {
                id = dbEntry.Property("Id").CurrentValue.ToString();
            }
            catch (Exception ex)
            {

            }

            //var originalValues = _dbSet.Find(id);
            string changes = "";
            string oldValues = "{";
            foreach (var property in dbEntry.OriginalValues.Properties)
            {
                try
                {
                    var originalValue = dbEntry.GetDatabaseValues().GetValue<object>(property.Name);
                    var currentValue = dbEntry.Property(property.Name).CurrentValue;

                    if (!Equals(originalValue, currentValue))
                    {
                        changes += property.Name + ": " + originalValue + " -> " + currentValue + "<br />";
                    }
                }
                catch (Exception ex)
                {
                }
            }

            var AuditTrail = new AuditTrail
            {
                ChangeType = "Update",
                Description = changes,
                TableName = entity.GetType().Name,
                RecordId = id,
            };

            _dbService.AuditTrail(AuditTrail);
        }

        public TEntity Find(int? id)
        {
            return _dbSet.Find(id);
        }

        public TEntity Find(string id)
        {
            return _dbSet.Find(id);
        }

        public IQueryable<TEntity> FindAll()
        {
            return GetQueryable();
        }

        public TEntity First()
        {
            return GetQueryable().First();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return GetQueryable().ToList();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await GetQueryableAsync();
        }

        public IQueryable<TEntity> Queryable()
        {
            return _dbSet;
        }

        public void Remove(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            _dbSet.RemoveRange(entities);
        }

        public TEntity SingleOrDefault()
        {
            return GetQueryable().SingleOrDefault();
        }

        public IEnumerable<TEntity> Where()
        {
            return GetQueryable().ToList();
        }

        /* :::::::: Internals ::::::::*/
        internal IQueryable<TEntity> GetQueryable()
        {
            IQueryable<TEntity> query = _dbSet;

            return query;
        }

        internal async Task<IEnumerable<TEntity>> GetQueryableAsync()
        {
            return await GetQueryable().ToListAsync();
        }
    }
}
