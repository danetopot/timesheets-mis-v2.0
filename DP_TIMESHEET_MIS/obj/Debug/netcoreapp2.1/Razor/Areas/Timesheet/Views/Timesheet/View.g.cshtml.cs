#pragma checksum "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\View.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7c9550c554299c2c8b942f3eaae8ed1bb4d691af"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Timesheet_Views_Timesheet_View), @"mvc.1.0.view", @"/Areas/Timesheet/Views/Timesheet/View.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/Timesheet/Views/Timesheet/View.cshtml", typeof(AspNetCore.Areas_Timesheet_Views_Timesheet_View))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\_ViewImports.cshtml"
using DP_TIMESHEET_MIS;

#line default
#line hidden
#line 2 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\_ViewImports.cshtml"
using DP_TIMESHEET_MIS.Models;

#line default
#line hidden
#line 1 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\View.cshtml"
using DP_TIMESHEET_MIS.ViewModels.Timesheet;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7c9550c554299c2c8b942f3eaae8ed1bb4d691af", @"/Areas/Timesheet/Views/Timesheet/View.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4963750cb275e435f549b1884294dca1496e3c15", @"/Areas/Timesheet/Views/_ViewImports.cshtml")]
    public class Areas_Timesheet_Views_Timesheet_View : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ViewTimesheetViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "hidden", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("HeaderId"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(47, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(81, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 5 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\View.cshtml"
  
    ViewData["Title"] = "TIMESHEET MIS | TIMESHEET";

#line default
#line hidden
            BeginContext(144, 1362, true);
            WriteLiteral(@"
<section role=""main"" class=""content-body"">
    <header class=""page-header custom-input-style"">
        <h2>Timesheet</h2>

        <div class=""right-wrapper pull-right"">
            <ol class=""breadcrumbs"">
                <li>
                    <a href=""index.html"">
                        <i class=""fa fa-home""></i>
                    </a>
                </li>
                <li><span class=""custom-input-style"">Timesheet</span></li>
                <li><span class=""custom-input-style"">View</span></li>
            </ol>

            <a class=""sidebar-right-toggle"" data-open=""sidebar-right""><i class=""fa fa-chevron-left""></i></a>
        </div>
    </header>
    <div class=""row"">
        <div class=""col-lg-12"">
            <section class=""panel"">
                <header class=""panel-heading"">
                    <div class=""panel-actions"">
                        <a href=""#"" class=""fa fa-caret-down""></a>
                        <a href=""#"" class=""fa fa-times""></a>
               ");
            WriteLiteral(@"     </div>

                    <h2 class=""panel-title custom-input-style"">View Timesheet</h2>
                    <p class=""panel-subtitle custom-input-style"">
                        Please view the open timesheet.
                    </p>
                </header>
                <div class=""panel-body"">
                    ");
            EndContext();
            BeginContext(1506, 78, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "4a814d809e184a22bff5ede5bc0a9596", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
#line 42 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\View.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.HeaderId);

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            BeginWriteTagHelperAttribute();
#line 42 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\View.cshtml"
                                                                     WriteLiteral(Model.HeaderId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.Value = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1584, 1227, true);
            WriteLiteral(@"

                    <div style=""height: 430px"">
                        <table id=""tblTimesheet"" border=""1"" class=""table table-bordered mb-none"">
                            <thead>
                                <tr>
                                    <th colspan=""6"" class=""table-header""></th>
                                    <th>DAY</th>
                                    <th><b>DATE WORKED</b></th>
                                    <th><b>SITE</b></th>
                                    <th><b>LOCATION</b></th>
                                    <th><b>TASK DESCRIPTION</b></th>
                                    <th><b>SITE</b></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <br />
                    <div style=""float: right"">
                        <a id=""btnPrint"" class=""btn btn-default custom-input-style"">
        ");
            WriteLiteral("                    <i class=\"fa fa-print\"></i> Print\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(2834, 415, true);
                WriteLiteral(@"
    <script type=""text/javascript"">
        function printData() {
            var divToPrint = document.getElementById(""tblTimesheet"");
            newWin = window.open("""");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close();
        }

        $('#btnPrint').on('click', function () {
            printData();
        })

    </script>
");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ViewTimesheetViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
