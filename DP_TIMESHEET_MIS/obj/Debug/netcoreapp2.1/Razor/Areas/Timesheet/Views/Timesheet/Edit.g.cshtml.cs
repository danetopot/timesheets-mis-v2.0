#pragma checksum "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "60190471ff1e0399a0a4bd782f7f6955585cacbb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Timesheet_Views_Timesheet_Edit), @"mvc.1.0.view", @"/Areas/Timesheet/Views/Timesheet/Edit.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/Timesheet/Views/Timesheet/Edit.cshtml", typeof(AspNetCore.Areas_Timesheet_Views_Timesheet_Edit))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\_ViewImports.cshtml"
using DP_TIMESHEET_MIS;

#line default
#line hidden
#line 2 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\_ViewImports.cshtml"
using DP_TIMESHEET_MIS.Models;

#line default
#line hidden
#line 1 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
using DP_TIMESHEET_MIS.ViewModels.Timesheet;

#line default
#line hidden
#line 2 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
using PagedList.Core.Mvc;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"60190471ff1e0399a0a4bd782f7f6955585cacbb", @"/Areas/Timesheet/Views/Timesheet/Edit.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4963750cb275e435f549b1884294dca1496e3c15", @"/Areas/Timesheet/Views/_ViewImports.cshtml")]
    public class Areas_Timesheet_Views_Timesheet_Edit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<EditTimesheetViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "New", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Timesheet", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-area", "Timesheet", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-success custom-input-style"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Search", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-default custom-input-style"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "hidden", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("HeaderId"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "EditDetail", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("custom-input-style"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_10 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "View", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_11 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("pager-container"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_12 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_13 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_14 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-inline form-paginated"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::PagedList.Core.Mvc.PagerTagHelper __PagedList_Core_Mvc_PagerTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(107, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 5 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
  
    ViewData["Title"] = "TIMESHEET MIS | TIMESHEET";

#line default
#line hidden
            BeginContext(170, 1663, true);
            WriteLiteral(@"
<section role=""main"" class=""content-body"">
    <header class=""page-header custom-input-style"">
        <h2>Timesheet</h2>

        <div class=""right-wrapper pull-right"">
            <ol class=""breadcrumbs"">
                <li>
                    <a href=""index.html"">
                        <i class=""fa fa-home""></i>
                    </a>
                </li>
                <li><span class=""custom-input-style"">Timesheet</span></li>
                <li><span class=""custom-input-style"">Edit</span></li>
            </ol>

            <a class=""sidebar-right-toggle"" data-open=""sidebar-right""><i class=""fa fa-chevron-left""></i></a>
        </div>
    </header>
    <div class=""row"">
        <div class=""col-lg-12"">
            <section class=""panel"">
                <header class=""panel-heading"">
                    <div class=""panel-actions"">
                        <a href=""#"" class=""fa fa-caret-down""></a>
                        <a href=""#"" class=""fa fa-times""></a>
               ");
            WriteLiteral(@"     </div>

                    <h2 class=""panel-title custom-input-style"">Edit Timesheet</h2>
                    <p class=""panel-subtitle custom-input-style"">
                        Please edit the open timesheet.
                    </p>
                </header>
                <header class=""panel-heading"" style=""background-color: white"">
                    <div class=""row"">
                        <div class=""form-group col-md-6"">
                            <label class=""control-label custom-input-style"">Name : </label>
                            <label class=""control-label custom-input-style text-primary""><b>");
            EndContext();
            BeginContext(1834, 10, false);
#line 45 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                                                                       Write(Model.Name);

#line default
#line hidden
            EndContext();
            BeginContext(1844, 290, true);
            WriteLiteral(@"</b></label>
                        </div>
                        <div class=""form-group col-md-6"">
                            <label class=""control-label custom-input-style"">Type : </label>
                            <label class=""control-label custom-input-style text-primary""><b>");
            EndContext();
            BeginContext(2135, 10, false);
#line 49 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                                                                       Write(Model.Type);

#line default
#line hidden
            EndContext();
            BeginContext(2145, 387, true);
            WriteLiteral(@"</b></label>
                        </div>
                    </div>
                    <br />
                    <div class=""row"">
                        <div class=""form-group col-md-6"">
                            <label class=""control-label custom-input-style"">Period : </label>
                            <label class=""control-label custom-input-style text-primary""><b>");
            EndContext();
            BeginContext(2533, 12, false);
#line 56 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                                                                       Write(Model.Period);

#line default
#line hidden
            EndContext();
            BeginContext(2545, 293, true);
            WriteLiteral(@"</b></label>
                        </div>
                        <div class=""form-group col-md-6"">
                            <label class=""control-label custom-input-style"">Project : </label>
                            <label class=""control-label custom-input-style text-primary""><b>");
            EndContext();
            BeginContext(2839, 13, false);
#line 60 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                                                                       Write(Model.Project);

#line default
#line hidden
            EndContext();
            BeginContext(2852, 218, true);
            WriteLiteral("</b></label>\r\n                        </div>\r\n                    </div>\r\n                </header>\r\n\r\n                <div class=\"panel-body\">\r\n\r\n                    <div style=\"float: left\">\r\n                        ");
            EndContext();
            BeginContext(3070, 211, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "82c2c053c283435d9ab63dd14794fea2", async() => {
                BeginContext(3181, 96, true);
                WriteLiteral("\r\n                            <i class=\"fa fa-plus\"></i> New Timesheet\r\n                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Area = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3281, 102, true);
            WriteLiteral("\r\n                    </div>\r\n                    <div style=\"float: right\">\r\n                        ");
            EndContext();
            BeginContext(3383, 245, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1c6b9bfdf81f493685a65b683dea4b04", async() => {
                BeginContext(3528, 96, true);
                WriteLiteral("\r\n                            <i class=\"fa fa-times\"></i>&nbsp; Cancel\r\n                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Area = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 73 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                                                                                 WriteLiteral(Model.HeaderId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3628, 106, true);
            WriteLiteral("\r\n                    </div>\r\n                    <br />\r\n                    <br />\r\n                    ");
            EndContext();
            BeginContext(3734, 78, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "727c0cb0e07b465982361c819c61ac1c", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
#line 79 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.HeaderId);

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_6.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
            BeginWriteTagHelperAttribute();
#line 79 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                                                     WriteLiteral(Model.HeaderId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.Value = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3812, 468, true);
            WriteLiteral(@"

                    <div style=""height: 750px"">
                        <table class=""table table-striped table-bordered table-responsive custom-input-style"">
                            <thead>
                                <tr><th colspan=""7"" class=""header-logo"" height=""100""></th></tr>
                                <tr>
                                    <th><b>DATE WORKED</b></th>
                                    <th><b>HOURS WORKED</b></th>
");
            EndContext();
            BeginContext(4345, 70, true);
            WriteLiteral("                                    <th><b>TASK DESCRIPTION</b></th>\r\n");
            EndContext();
            BeginContext(4477, 344, true);
            WriteLiteral(@"                                    <th><b>SITE</b></th>
                                    <th style=""text-align: center;""><b>EDIT</b></th>
                                    <th style=""text-align: center;""><b>VIEW</b></th>
                                </tr>
                            </thead>
                            <tbody>
");
            EndContext();
#line 97 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                  int i = 1;

#line default
#line hidden
            BeginContext(4868, 32, true);
            WriteLiteral("                                ");
            EndContext();
#line 98 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                 foreach (var item in Model.Timesheets)
                                {
                                    var _detailId = item.Id;


#line default
#line hidden
            BeginContext(5040, 86, true);
            WriteLiteral("                                    <tr>\r\n                                        <td>");
            EndContext();
            BeginContext(5127, 45, false);
#line 103 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.DateFilled));

#line default
#line hidden
            EndContext();
            BeginContext(5172, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(5224, 46, false);
#line 104 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.HoursWorked));

#line default
#line hidden
            EndContext();
            BeginContext(5270, 7, true);
            WriteLiteral("</td>\r\n");
            EndContext();
            BeginContext(5396, 44, true);
            WriteLiteral("                                        <td>");
            EndContext();
            BeginContext(5441, 50, false);
#line 106 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.TaskDescription));

#line default
#line hidden
            EndContext();
            BeginContext(5491, 7, true);
            WriteLiteral("</td>\r\n");
            EndContext();
            BeginContext(5598, 44, true);
            WriteLiteral("                                        <td>");
            EndContext();
            BeginContext(5643, 44, false);
#line 108 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Site.Code));

#line default
#line hidden
            EndContext();
            BeginContext(5687, 112, true);
            WriteLiteral("</td>\r\n                                        <td align=\"center\">\r\n                                            ");
            EndContext();
            BeginContext(5799, 254, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "df12b32c60954da3afed35a3c15b7741", async() => {
                BeginContext(5927, 122, true);
                WriteLiteral("\r\n                                                <i class=\"fa fa-edit\"></i>\r\n                                            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_8.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_8);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Area = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 110 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                                                                                                         WriteLiteral(_detailId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6053, 154, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td align=\"center\">\r\n                                            ");
            EndContext();
            BeginContext(6207, 253, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "017c01eb9806431da2df9ffca9c11f41", async() => {
                BeginContext(6329, 127, true);
                WriteLiteral("\r\n                                                <i class=\"fa fa-eye-slash\"></i>\r\n                                            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_10.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_10);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Area = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 115 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                                                                                                   WriteLiteral(_detailId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6460, 92, true);
            WriteLiteral("\r\n                                        </td>\r\n                                    </tr>\r\n");
            EndContext();
#line 120 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                    i++;
                                }

#line default
#line hidden
            BeginContext(6629, 129, true);
            WriteLiteral("                            </tbody>\r\n                        </table>\r\n                        <br />\r\n                         ");
            EndContext();
            BeginContext(6758, 464, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d77fac42ca584ce2bb4e296342ad5ea0", async() => {
                BeginContext(6814, 30, true);
                WriteLiteral("\r\n                            ");
                EndContext();
                BeginContext(6844, 249, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("pager", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "93af7cc11a0b4d0494441da5ec9dd5d6", async() => {
                }
                );
                __PagedList_Core_Mvc_PagerTagHelper = CreateTagHelper<global::PagedList.Core.Mvc.PagerTagHelper>();
                __tagHelperExecutionContext.Add(__PagedList_Core_Mvc_PagerTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_11);
#line 126 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
__PagedList_Core_Mvc_PagerTagHelper.List = Model.Timesheets;

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("list", __PagedList_Core_Mvc_PagerTagHelper.List, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#line 126 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
__PagedList_Core_Mvc_PagerTagHelper.Options = PagedListRenderOptions.TwitterBootstrapPager;

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("options", __PagedList_Core_Mvc_PagerTagHelper.Options, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __PagedList_Core_Mvc_PagerTagHelper.AspAction = (string)__tagHelperAttribute_12.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_12);
                __PagedList_Core_Mvc_PagerTagHelper.AspController = (string)__tagHelperAttribute_1.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
                if (__PagedList_Core_Mvc_PagerTagHelper.RouteValues == null)
                {
                    throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-keyword", "PagedList.Core.Mvc.PagerTagHelper", "RouteValues"));
                }
                BeginWriteTagHelperAttribute();
#line 128 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Edit.cshtml"
                                   WriteLiteral(Model.Page);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __PagedList_Core_Mvc_PagerTagHelper.RouteValues["keyword"] = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-keyword", __PagedList_Core_Mvc_PagerTagHelper.RouteValues["keyword"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(7093, 122, true);
                WriteLiteral("\r\n                            <input type=\"hidden\" name=\"Page\" id=\"pagination-page\" value=\"1\" />\r\n                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_13.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_13);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_14);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(7222, 120, true);
            WriteLiteral("\r\n                    </div>\r\n\r\n                </div>\r\n            </section>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<EditTimesheetViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
