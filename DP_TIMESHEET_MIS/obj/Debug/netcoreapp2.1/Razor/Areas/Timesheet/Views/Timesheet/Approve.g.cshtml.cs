#pragma checksum "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c7a3bd08c2b07c81c6def19798edc1be54688d4a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Timesheet_Views_Timesheet_Approve), @"mvc.1.0.view", @"/Areas/Timesheet/Views/Timesheet/Approve.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/Timesheet/Views/Timesheet/Approve.cshtml", typeof(AspNetCore.Areas_Timesheet_Views_Timesheet_Approve))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\_ViewImports.cshtml"
using DP_TIMESHEET_MIS;

#line default
#line hidden
#line 2 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\_ViewImports.cshtml"
using DP_TIMESHEET_MIS.Models;

#line default
#line hidden
#line 1 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
using DP_TIMESHEET_MIS.ViewModels.Timesheet;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c7a3bd08c2b07c81c6def19798edc1be54688d4a", @"/Areas/Timesheet/Views/Timesheet/Approve.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4963750cb275e435f549b1884294dca1496e3c15", @"/Areas/Timesheet/Views/_ViewImports.cshtml")]
    public class Areas_Timesheet_Views_Timesheet_Approve : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ApproveTimesheetViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Approving", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-success custom-input-style"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("RejectReason"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-control"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rows", new global::Microsoft.AspNetCore.Html.HtmlString("5"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("placeholder", new global::Microsoft.AspNetCore.Html.HtmlString("Type your reason"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Search", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Timesheet", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-area", "Timesheet", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-default custom-input-style"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.TextAreaTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_TextAreaTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(47, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(84, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 5 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
  
    ViewData["Title"] = "TIMESHEET MIS | APPROVE TIMESHEET";

#line default
#line hidden
            BeginContext(155, 1679, true);
            WriteLiteral(@"
<section role=""main"" class=""content-body"">
    <header class=""page-header custom-input-style"">
        <h2>Timesheet</h2>

        <div class=""right-wrapper pull-right"">
            <ol class=""breadcrumbs"">
                <li>
                    <a href=""index.html"">
                        <i class=""fa fa-home""></i>
                    </a>
                </li>
                <li><span class=""custom-input-style"">Timesheet</span></li>
                <li><span class=""custom-input-style"">Approve</span></li>
            </ol>

            <a class=""sidebar-right-toggle"" data-open=""sidebar-right""><i class=""fa fa-chevron-left""></i></a>
        </div>
    </header>
    <div class=""row"">
        <div class=""col-lg-12"">
            <section class=""panel"">
                <header class=""panel-heading"">
                    <div class=""panel-actions"">
                        <a href=""#"" class=""fa fa-caret-down""></a>
                        <a href=""#"" class=""fa fa-times""></a>
            ");
            WriteLiteral(@"        </div>

                    <h2 class=""panel-title custom-input-style"">Approve Timesheet</h2>
                    <p class=""panel-subtitle custom-input-style"">
                        Please approve/reject the open timesheet.
                    </p>
                </header>
                <header class=""panel-heading"" style=""background-color: white"">
                    <div class=""row"">
                        <div class=""form-group col-md-6"">
                            <label class=""control-label custom-input-style"">Name : </label>
                            <label class=""control-label custom-input-style text-primary""><b>");
            EndContext();
            BeginContext(1835, 10, false);
#line 45 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                                                                       Write(Model.Name);

#line default
#line hidden
            EndContext();
            BeginContext(1845, 290, true);
            WriteLiteral(@"</b></label>
                        </div>
                        <div class=""form-group col-md-6"">
                            <label class=""control-label custom-input-style"">Type : </label>
                            <label class=""control-label custom-input-style text-primary""><b>");
            EndContext();
            BeginContext(2136, 10, false);
#line 49 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                                                                       Write(Model.Type);

#line default
#line hidden
            EndContext();
            BeginContext(2146, 387, true);
            WriteLiteral(@"</b></label>
                        </div>
                    </div>
                    <br />
                    <div class=""row"">
                        <div class=""form-group col-md-6"">
                            <label class=""control-label custom-input-style"">Period : </label>
                            <label class=""control-label custom-input-style text-primary""><b>");
            EndContext();
            BeginContext(2534, 12, false);
#line 56 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                                                                       Write(Model.Period);

#line default
#line hidden
            EndContext();
            BeginContext(2546, 293, true);
            WriteLiteral(@"</b></label>
                        </div>
                        <div class=""form-group col-md-6"">
                            <label class=""control-label custom-input-style"">Project : </label>
                            <label class=""control-label custom-input-style text-primary""><b>");
            EndContext();
            BeginContext(2840, 13, false);
#line 60 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                                                                       Write(Model.Project);

#line default
#line hidden
            EndContext();
            BeginContext(2853, 214, true);
            WriteLiteral("</b></label>\r\n                        </div>\r\n                    </div>\r\n                </header>\r\n                <div class=\"panel-body\">\r\n                    <div style=\"float: left\">\r\n                        ");
            EndContext();
            BeginContext(3067, 167, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e9c7e0314486437aa0380ad83724a049", async() => {
                BeginContext(3167, 63, true);
                WriteLiteral("\r\n                            Approve\r\n                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 66 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                                    WriteLiteral(Model.HeaderId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3234, 1082, true);
            WriteLiteral(@"
                    </div>
                    <div style=""float: right"">
                        <a class=""mb-xs mt-xs mr-xs modal-basic btn btn-danger custom-input-style"" href=""#modalReject"">Reject</a>
                    </div>
                    <hr />

                    <div id=""modalReject"" class=""modal-block modal-header-color modal-block-danger mfp-hide"">
                        <section class=""panel"">
                            <header class=""panel-heading custom-input-style"">
                                <h2 class=""panel-title"">Reject Reason</h2>
                            </header>
                            <div class=""panel-body"">
                                <div class=""row"">
                                    <div class=""col-sm-12"">
                                        <div class=""form-group custom-input-style"">
                                            <label class=""control-label"">Reason for Rejecting Timesheet </label>
                                      ");
            WriteLiteral("      <br />\r\n                                            ");
            EndContext();
            BeginContext(4316, 123, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("textarea", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "11361fdbc23344ea8a99320a29bce5c9", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_TextAreaTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.TextAreaTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_TextAreaTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
#line 86 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_TextAreaTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.RejectReason);

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_TextAreaTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4439, 1586, true);
            WriteLiteral(@"
                                        </div>
                                    </div>



                                </div>
                            </div>
                            <footer class=""panel-footer"">
                                <div class=""row"">
                                    <div class=""col-md-12 text-right"">
                                        <button id=""btnSaveRejectReason"" class=""btn btn-info modal-dismiss custom-input-style"">Save</button>
                                        <button class=""btn btn-default modal-dismiss custom-input-style"">Cancel</button>
                                    </div>
                                </div>
                            </footer>

                        </section>

                    </div>
                    <div id=""tblTimesheet"" style=""height: auto"">
                        <table class=""table table-striped table-bordered table-responsive custom-input-style"">
                            <thea");
            WriteLiteral(@"d>
                                <tr><th colspan=""5"" class=""header-logo"" height=""100""></th></tr>
                                <tr>
                                    <th>DAY</th>
                                    <th><b>DATE WORKED</b></th>
                                    <th><b>HOURS WORKED</b></th>
                                    <th><b>TASK DESCRIPTION</b></th>
                                    <th><b>SITE</b></th>
                                </tr>
                            </thead>
                            <tbody>
");
            EndContext();
#line 119 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                  int i = 1;

#line default
#line hidden
            BeginContext(6072, 32, true);
            WriteLiteral("                                ");
            EndContext();
#line 120 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                 foreach (var item in Model.Timesheets)
                                {

#line default
#line hidden
            BeginContext(6180, 86, true);
            WriteLiteral("                                    <tr>\r\n                                        <td>");
            EndContext();
            BeginContext(6267, 1, false);
#line 123 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                       Write(i);

#line default
#line hidden
            EndContext();
            BeginContext(6268, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(6320, 45, false);
#line 124 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.DateFilled));

#line default
#line hidden
            EndContext();
            BeginContext(6365, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(6417, 46, false);
#line 125 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.HoursWorked));

#line default
#line hidden
            EndContext();
            BeginContext(6463, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(6515, 50, false);
#line 126 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.TaskDescription));

#line default
#line hidden
            EndContext();
            BeginContext(6565, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(6617, 44, false);
#line 127 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Site.Code));

#line default
#line hidden
            EndContext();
            BeginContext(6661, 50, true);
            WriteLiteral("</td>\r\n                                    </tr>\r\n");
            EndContext();
#line 129 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                    i++;
                                }

#line default
#line hidden
            BeginContext(6788, 202, true);
            WriteLiteral("                            </tbody>\r\n                        </table>\r\n                    </div>\r\n\r\n                    <br />\r\n                    <div style=\"float: right\">\r\n                        ");
            EndContext();
            BeginContext(6990, 245, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c2c2f3142b14485ca263683b95eaaadc", async() => {
                BeginContext(7135, 96, true);
                WriteLiteral("\r\n                            <i class=\"fa fa-times\"></i>&nbsp; Cancel\r\n                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_6.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_7.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Area = (string)__tagHelperAttribute_8.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_8);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 137 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                                                                                                 WriteLiteral(Model.HeaderId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(7235, 124, true);
            WriteLiteral("\r\n                    </div>\r\n                </div>\r\n            </section>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(7382, 185, true);
                WriteLiteral("\r\n    <script type=\"text/javascript\">\r\n        $(document).ready(function () {\r\n\r\n\r\n        });\r\n\r\n        $(\"#btnSaveRejectReason\").click(function () {\r\n            var _headerId = \"\\\"");
                EndContext();
                BeginContext(7568, 14, false);
#line 158 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                          Write(Model.HeaderId);

#line default
#line hidden
                EndContext();
                BeginContext(7582, 6, true);
                WriteLiteral("\\\"\";\r\n");
                EndContext();
                BeginContext(7646, 264, true);
                WriteLiteral(@"            var _rejectReason = $('#RejectReason').val();
            var data = {
                'headerId': _headerId,
                'rejectReason': _rejectReason
            }

            $.ajax({
                type: ""POST"",
                url: '");
                EndContext();
                BeginContext(7911, 20, false);
#line 168 "C:\PROJECTS\TIMESHEET_MIS\DP_TIMESHEET_MIS\Areas\Timesheet\Views\Timesheet\Approve.cshtml"
                 Write(Url.Action("Reject"));

#line default
#line hidden
                EndContext();
                BeginContext(7931, 1055, true);
                WriteLiteral(@"',
                contentType: ""application/json"",
                dataType: ""json"",
                data: JSON.stringify(data),
                success: function (response) {
                    window.location.href = response;
                },
                error: function (xhr, status, error) {
                    alert(""Error Response :- "" + xhr.responseText);
                }
            });
        });

        $(""#btnPrint"").click(function () {
            var table = $(""#tblTimesheet"");
            win = window.open("""");
            win.document.write(table.outerHTML);
            win.print();
            win.close();
        });

        $(""#btnPrint1"").click(function () {
            printData();
        });


        function printData()
        {
            var divToPrint = document.getElementById(""printTable"");
            newWin = window.open("""");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close()");
                WriteLiteral(";\r\n        }\r\n\r\n    </script>\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ApproveTimesheetViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
